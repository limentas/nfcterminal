QT += quick svg
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG(release, debug|release) {
  BuildType = "release"
}
CONFIG(debug, debug|release) {
  BuildType = "debug"
}

INCLUDEPATH += ../

LIBS += -L../CommonLib/$${BuildType}
LIBS += -L../../CommonLib/$${BuildType}
LIBS += -L../../../CommonLib/$${BuildType}
LIBS += -L../../../../CommonLib/$${BuildType}
LIBS += -L../../../../../CommonLib/$${BuildType}
#LIBS += -L"D:\Projects\builds\build-Desktop_Qt_5_10_0_MinGW_32bit\CommonLib\debug"
#LIBS += -L"D:\Projects\builds\build-Android_x86_Qt5100_GCC49\CommonLib\debug"
LIBS += -lCommonLib

win32: CONFIG += no_extras

# for debugging on PC
no_extras {
    DEFINES += DO_NOT_USE_ANDROID_EXTRAS
    DEFINES += USE_STUBS
}
else {
    QT += androidextras
    #DEFINES += USE_STUBS
}

SOURCES += \
    exchangelogmodel.cpp \
    jnihandlers.cpp \
    jnimethods.cpp \
    main.cpp \
    nfcterminalapp.cpp \
    cardinfo.cpp \
    javanfcreader.cpp \
    favoritecommandsmodel.cpp

RESOURCES += qml/qml.qrc \
    img/img.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    exceptions.hpp \
    exchangelogmodel.h \
    ireader.hpp \
    jnimethods.h \
    nfcterminalapp.h \
    responseerror.hpp \
    cardinfo.h \
    javanfcreader.h \
    enums.hpp \
    favoritecommandsmodel.h

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/values/libs.xml \
    android/res/xml/filter_nfc.xml \
    android/src/NfcTerminalActivity.java \
    android/src/NativeFunctions.java \
    android/src/AppLogReader.java \
    android/src/UncaughtExceptionHandler.java


# - setup the correct location to install to and load from
android {
    # android platform
    # From: http://community.kde.org/Necessitas/Assets
    LICENSES_INSTALL_PATH=/assets/Licenses
} else {
    # other platforms
    LICENSES_INSTALL_PATH=$$OUT_PWD/Licenses
}

LICENSES_FILES = \
    deploy/icons-apache-license.txt \
    deploy/taurus-mono-outline-font-OFL.txt \
    deploy/NOTICE.txt

# - setup the 'make install' step
licenses.path = $$LICENSES_INSTALL_PATH
licenses.files += $$LICENSES_FILES
licenses.depends += FORCE

INSTALLS += licenses

contains(ANDROID_TARGET_ARCH,x86) {
    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android
    ANDROID_EXTRA_LIBS = \
        $$PWD/../builds/android-x86-qt5_12_2-clang/CommonLib/release/libCommonLib.so
}

contains(ANDROID_TARGET_ARCH,armeabi-v7a) {
    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android
    ANDROID_EXTRA_LIBS = \
        $$PWD/../builds/android-armv7-qt5_12_2-clang/CommonLib/release/libCommonLib.so
}

contains(ANDROID_TARGET_ARCH,arm64-v8a) {
    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android
    ANDROID_EXTRA_LIBS = \
        $$PWD/../builds/android-arm64_v8a-qt5_12_2-clang/CommonLib/release/libCommonLib.so
}
