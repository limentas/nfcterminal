#ifndef RESPONSEERROR_HPP
#define RESPONSEERROR_HPP

namespace NfcTerminal {

enum class ResponseError
{
    CardLost,
    OtherError
};

} // NfcTerminal namespace


#endif // RESPONSEERROR_HPP
