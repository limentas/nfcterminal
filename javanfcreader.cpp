#include "javanfcreader.h"
#include "jnimethods.h"
#include "nfcterminalapp.h"
#include "enums.hpp"
#include "exceptions.hpp"

#include <QByteArray>

namespace NfcTerminal {

JavaNfcReader::JavaNfcReader()
{

}

QByteArray JavaNfcReader::tranceive(const QByteArray &request, TagTechnologies tech)
{
    NfcTerminalApp::logExchange(ExchangeDirection::ToCard, tech, QDateTime::currentDateTime(), request);
    NfcTerminalApp::logMsg(QString("Tranceive %1: %2").arg(QtEnumToString(tech))
                           .arg(QString::fromLatin1(request.toHex())));
    try
    {
        auto output = JniMethods::requestTransceive(request, tech);

        NfcTerminalApp::logExchange(ExchangeDirection::FromCard, QDateTime::currentDateTime(), output);
        NfcTerminalApp::logMsg(QString::fromLatin1(output.toHex()));
        return output;
    }
    catch (const CardReadException &ex)
    {
        NfcTerminalApp::logResponseError(QDateTime::currentDateTime(), ResponseError::OtherError);
        NfcTerminalApp::logMsg(QString("CardReadException"));
    }
    catch (const CardLostException &ex)
    {
        NfcTerminalApp::logResponseError(QDateTime::currentDateTime(), ResponseError::CardLost);
        NfcTerminalApp::logMsg(QString("CardLostException"));
    }
    return QByteArray();
}

} // NfcTerminal namespace
