// android/src/AppLogReader.java
package dev.slebe.nfcterminal;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.lang.Runtime;
import java.lang.Process;
import android.util.Log;

public final class AppLogReader {
    private static final String LOG_TAG = "NfcTerminalActivity";
    private static final String processId = Integer.toString(android.os.Process
                .myPid());

    public static StringBuilder getLog() {
        StringBuilder builder = new StringBuilder();

        try
        {
            String[] command = new String[] { "logcat", "-d", "-v", "threadtime" };

            Process process = Runtime.getRuntime().exec(command);

            BufferedReader bufferedReader = new BufferedReader(
                          new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                //if (line.contains(processId)) {
                    builder.append(line);
                    builder.append("\n");
                //}
            }
        } catch (IOException ex) {
            Log.e(LOG_TAG, "Getting logs failed", ex);
        }

      return builder;
  }
}
