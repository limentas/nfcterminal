// android/src/NfcTerminalActivity.java
package dev.slebe.nfcterminal;

import org.qtproject.qt5.android.bindings.QtActivity;

import android.os.Bundle;
import android.content.Intent;
import android.app.PendingIntent;
import android.provider.Settings;

import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcB;
import android.nfc.tech.NfcF;
import android.nfc.tech.NfcV;
import android.nfc.tech.IsoDep;
import android.nfc.tech.Ndef;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.NfcBarcode;
import android.nfc.TagLostException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.Context;

import android.util.Log;
import android.view.KeyEvent;
import android.os.Parcel;
import android.os.IBinder;
import android.os.Handler;
import java.lang.Runnable;
import java.io.IOException;

public class NfcTerminalActivity extends QtActivity
{
    private static NfcAdapter nfcAdapter_ = null;
    private static ConnectivityManager connectManager_ = null;
    private static IsoDep    isoTag_ = null;
    private static NfcA      nfcaTag_ = null;
    private static NfcB      nfcbTag_ = null;
    private static NfcTerminalActivity instance_ = null;
    private static PackageInfo packageInfo_ = null;
    private static final int CARD_PING_TIMEOUT = 300;
    private static final String LOG_TAG = "NfcTerminal";

    private static Handler timerHandler = new Handler();
    private static Runnable timerRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            boolean isConnected = false;
            if (nfcaTag_ != null && !isConnected)
            {
                isConnected = nfcaTag_.isConnected();
            }
            if (nfcbTag_ != null && !isConnected)
            {
                isConnected = nfcbTag_.isConnected();
            }
            if (isoTag_ != null && !isConnected)
            {
                isConnected = isoTag_.isConnected();
            }

            if (isConnected)
            {
                timerHandler.postDelayed(this, CARD_PING_TIMEOUT);
            }
            else
            {
                Log.d(LOG_TAG, "Timer handler: tag was lost");
                NativeFunctions.tagLostNotice();
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        Log.d(LOG_TAG, "onCreate()");
        Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler());
        super.onCreate(savedInstanceState);
        instance_ = this;
        nfcAdapter_ = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter_ == null)
        {
            Log.d(LOG_TAG, "onCreate() nfcNotSupportedNotice");
            NativeFunctions.nfcNotSupportedNotice();
        }
        else if (!nfcAdapter_.isEnabled())
        {
            Log.d(LOG_TAG, "onCreate() nfcEnabledNotice false");
            NativeFunctions.nfcEnabledNotice(false);
        }

        connectManager_ = (ConnectivityManager)
        getSystemService(Context.CONNECTIVITY_SERVICE);

        try
        {
            Context context = getApplicationContext();
            packageInfo_ = context.getPackageManager().getPackageInfo(context.getPackag‌​eName(), 0);
        }
        catch(NameNotFoundException e)
        {
            packageInfo_ = null;
        }
    }

    @Override
    public void onResume()
    {
        Log.d(LOG_TAG, "onResume()");
        super.onResume();
        Intent intent = getIntent();
        if (intent != null)
        {
            Log.d(LOG_TAG, "onResume() " + intent.getAction());
            processIntent(intent);
        }

        if (nfcAdapter_ != null)
        {
            if (nfcAdapter_.isEnabled())
            {
                NativeFunctions.nfcEnabledNotice(true);
                Intent launchedIntent = new Intent(this, this.getClass()).addFlags(
                    Intent.FLAG_ACTIVITY_SINGLE_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(
                    this, 0, launchedIntent, 0);
                nfcAdapter_.enableForegroundDispatch(
                    this, pendingIntent, null, new String[][]
                    {
                        new String[] { NfcA.class.getName() },
                    });
            }
            else
            {
                Log.d(LOG_TAG, "onResume() nfcEnabledNotice false");
                NativeFunctions.nfcEnabledNotice(false);
            }
        }
    }

    @Override
    public void onPause()
    {
        Log.d(LOG_TAG, "onPause()");
        super.onPause();
        timerHandler.removeCallbacks(timerRunnable);
        if (nfcAdapter_ != null && nfcAdapter_.isEnabled())
        {
            nfcAdapter_.disableForegroundDispatch(this);
        }
    }

    @Override
    public void onNewIntent(Intent intent)
    {
        Log.d(LOG_TAG, "onNewIntent() " + intent.getAction());
        setIntent(intent);
        super.onNewIntent(intent);
    }

    @Override
    public void onBackPressed()
    {
        NativeFunctions.backButtonClickedNotice();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (/*Integer.parseInt(android.os.Build.VERSION.SDK) < 5
            && */keyCode == KeyEvent.KEYCODE_BACK
            && event.getRepeatCount() == 0)
        {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public static byte[] requestNfcA(byte[] request) throws IOException, TagLostException
    {
        if (nfcaTag_ != null)
        {
            int attempts_count = 5;
            int attemptNumber = 0;
            boolean tagWasLost = false;
            boolean transcieveError = false;
            while (++attemptNumber <= attempts_count)
            {
                try
                {
                    connectToNfcATag();
                    nfcaTag_.setTimeout(1000);
                    byte[] res = nfcaTag_.transceive(request);
                    return res;
                }
                catch(TagLostException except)
                {
                    Log.d(LOG_TAG, "TagLostException " + except.toString());
                    tagWasLost = true;
                }
                catch(IOException except)
                {
                    Log.d(LOG_TAG, "IOException ", except);
                    transcieveError = true;
                }
            }
            if (tagWasLost)
                throw new TagLostException();
            else if (transcieveError)
                throw new IOException("Transceive failed");
        }
        return null;
    }

    public static byte[] requestNfcB(byte[] request) throws IOException, TagLostException
    {
        if (nfcbTag_ != null)
        {
            int attempts_count = 5;
            int attemptNumber = 0;
            boolean tagWasLost = false;
            boolean transcieveError = false;
            while (++attemptNumber <= attempts_count)
            {
                try
                {
                    connectToNfcBTag();
                    byte[] res = nfcbTag_.transceive(request);
                    return res;
                }
                catch(TagLostException except)
                {
                    Log.d(LOG_TAG, "TagLostException " + except.toString());
                    tagWasLost = true;
                }
                catch(IOException except)
                {
                    Log.d(LOG_TAG, "IOException ", except);
                    transcieveError = true;
                }
            }
            if (tagWasLost)
                throw new TagLostException();
            else if (transcieveError)
                throw new IOException("Transceive failed");
        }
        return null;
    }

    public static byte[] requestIso(byte[] request) throws IOException, TagLostException
    {
        if (isoTag_ != null)
        {
            int attempts_count = 5;
            int attemptNumber = 0;
            boolean tagWasLost = false;
            boolean transcieveError = false;
            while (++attemptNumber <= attempts_count)
            {
                try
                {
                    connectToIsoTag();
                    isoTag_.setTimeout(1000);
                    byte[] res = isoTag_.transceive(request);
                    return res;
                }
                catch(TagLostException except)
                {
                    Log.d(LOG_TAG, "TagLostException " + except.toString());
                    tagWasLost = true;
                }
                catch(IOException except)
                {
                    Log.d(LOG_TAG, "IOException ", except);
                    transcieveError = true;
                }
            }
            if (tagWasLost)
                throw new TagLostException();
            else if (transcieveError)
                throw new IOException("Transceive failed");
        }
        return null;
    }

    public static boolean isNfcSupported()
    {
        return nfcAdapter_ != null;
    }

    public static boolean isNfcEnabled()
    {
        if (nfcAdapter_ == null)
            return false;
        return nfcAdapter_.isEnabled();
    }

    public static boolean isInternetAvailable()
    {
        if (connectManager_ == null)
            return false;
        NetworkInfo netInfo = connectManager_.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    public static void requestEnableNfc()
    {
        if (instance_ != null)
            instance_.requestEnableNfcPrivate();
    }

    public static long getAppInstallTime()
    {
        if (packageInfo_ != null)
            return packageInfo_.firstInstallTime;
        return 0;
    }

    public static void logcat(String msg)
    {
        Log.d(LOG_TAG, msg);
    }

    public static String takeLogs()
    {
        return AppLogReader.getLog().toString();
    }

    public static void requestTagsOnStartup()
    {
        if (isoTag_ != null || nfcaTag_ != null || nfcbTag_ != null)
        {
            int techs = (isoTag_ != null ? NativeFunctions.ISO14443_4 : 0)
                    | (nfcaTag_ != null ? NativeFunctions.ISO14443_3A : 0)
                    | (nfcbTag_ != null ? NativeFunctions.ISO14443_3B : 0);
            Log.d(LOG_TAG, "requestTagsOnStartup techs=" + techs);

            try
            {
                if (nfcaTag_ != null)
                    connectToNfcATag();
                else if (nfcbTag_ != null)
                    connectToNfcBTag();
                else if (nfcbTag_ != null)
                    connectToIsoTag();
            }
            catch(IOException except)
            {
                Log.e(LOG_TAG, "IOException ", except);
                return;
            }

            byte[] id = isoTag_ != null ? isoTag_.getTag().getId() : null;
            byte[] atqa = nfcaTag_ != null ? nfcaTag_.getAtqa() : null;
            short sak = nfcaTag_ != null ? nfcaTag_.getSak() : null;
            byte[] historicalBytes = isoTag_ != null ? isoTag_.getHistoricalBytes() : null;
            NativeFunctions.tagDiscoveredNotice(techs, id, atqa, sak, historicalBytes);
            timerHandler.postDelayed(timerRunnable, CARD_PING_TIMEOUT);
        }
    }

    private static void connectToNfcATag() throws IOException
    {
        if (nfcaTag_ == null || nfcaTag_.isConnected())
            return;

        if (nfcbTag_ != null && nfcbTag_.isConnected())
            nfcbTag_.close();
        if (isoTag_ != null && isoTag_.isConnected())
            isoTag_.close();

        nfcaTag_.connect();
        Log.d(LOG_TAG, "connectToNfcATag() nfcaTag_ getMaxTransceiveLength = " + nfcaTag_.getMaxTransceiveLength());
    }

    private static void connectToNfcBTag() throws IOException
    {
        if (nfcbTag_ == null || nfcbTag_.isConnected())
            return;

        if (nfcaTag_ != null && nfcaTag_.isConnected())
            nfcaTag_.close();
        if (isoTag_ != null && isoTag_.isConnected())
            isoTag_.close();

        nfcbTag_.connect();
    }

    private static void connectToIsoTag() throws IOException
    {
        if (isoTag_ == null || isoTag_.isConnected())
            return;

        if (nfcaTag_ != null && nfcaTag_.isConnected())
            nfcaTag_.close();
        if (nfcbTag_ != null && nfcbTag_.isConnected())
            nfcbTag_.close();

        isoTag_.connect();
    }

    private void requestEnableNfcPrivate()
    {
        startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
    }

    private void processIntent(Intent intent)
    {
        if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(intent.getAction())
            || NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction()))
        {
            disconnectAllTags();

            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

            if (tag == null)
            {
                Log.e(LOG_TAG, "tag == null");
                return;
            }

            {
                String[] technologies = tag.getTechList();
                StringBuilder builder = new StringBuilder();
                for(String s : technologies)
                    builder.append(s).append(", ");
                String str = builder.toString();
                Log.d(LOG_TAG, "processIntent() technologies " + str);
            }

            try
            {
                isoTag_ = IsoDep.get(tag);
                nfcaTag_ = NfcA.get(tag);
                nfcbTag_ = NfcB.get(tag);
            }
            catch (NullPointerException except1)
            {
                Log.e(LOG_TAG, "NfcA.get NullPointerException 1", except1);

                tag = patchTag(tag); //Trying to patch tag
                isoTag_ = IsoDep.get(tag);
                nfcaTag_ = NfcA.get(tag);
                nfcbTag_ = NfcB.get(tag);
                //If it will be NullPointerException we will send premortem feedback
            }

            try
            {
                if (nfcaTag_ != null)
                    connectToNfcATag();
                else if (nfcbTag_ != null)
                    connectToNfcBTag();
                else if (isoTag_ != null)
                    connectToIsoTag();
            }
            catch(IOException except)
            {
                Log.e(LOG_TAG, "IOException ", except);
                return;
            }

            int techs = (isoTag_ != null ? NativeFunctions.ISO14443_4 : 0)
                              | (nfcaTag_ != null ? NativeFunctions.ISO14443_3A : 0)
                              | (nfcbTag_ != null ? NativeFunctions.ISO14443_3B : 0);
            byte[] id = isoTag_ != null ? isoTag_.getTag().getId() : null;
            byte[] atqa = nfcaTag_ != null ? nfcaTag_.getAtqa() : null;
            short sak = nfcaTag_ != null ? nfcaTag_.getSak() : 0;
            byte[] historicalBytes = isoTag_ != null ? isoTag_.getHistoricalBytes() : null;
            NativeFunctions.tagDiscoveredNotice(techs, id, atqa, sak, historicalBytes);
            timerHandler.postDelayed(timerRunnable, CARD_PING_TIMEOUT);
            return;
        }
    }

    private void disconnectAllTags()
    {
        if (isoTag_ != null)
        {
            if (isoTag_.isConnected())
            {
                try
                {
                    isoTag_.close();
                }
                catch(IOException except)
                {
                    Log.d(LOG_TAG, "IOException ", except);
                }
            }
            isoTag_ = null;
        }

        if (nfcaTag_ != null)
        {
            if (nfcaTag_.isConnected())
            {
                try
                {
                    nfcaTag_.close();
                }
                catch(IOException except)
                {
                    Log.d(LOG_TAG, "IOException ", except);
                }
            }
            nfcaTag_ = null;
        }

        if (nfcbTag_ != null)
        {
            if (nfcbTag_.isConnected())
            {
                try
                {
                    nfcbTag_.close();
                }
                catch(IOException except)
                {
                    Log.d(LOG_TAG, "IOException ", except);
                }
            }
            nfcbTag_ = null;
        }
    }

    //This code is almost from https://github.com/ikarus23/MifareClassicTool/issues/52
    //I send the rays of good to these people
    private Tag patchTag(Tag tag)
    {
        if (tag == null)
        {
            return null;
        }

        String[] techList = tag.getTechList();
        for (int i = 0; i < techList.length; ++i)
        {
            Log.d(LOG_TAG, "patchTag techList[" + i + "] = " + techList[i]);
        }

        Parcel oldParcel = Parcel.obtain();
        tag.writeToParcel(oldParcel, 0);
        oldParcel.setDataPosition(0);

        int len = oldParcel.readInt();
        byte[] id = new byte[0];
        if (len >= 0)
        {
            id = new byte[len];
            oldParcel.readByteArray(id);
        }
        int[] oldTechList = new int[oldParcel.readInt()];
        oldParcel.readIntArray(oldTechList);
        for (int i = 0; i < oldTechList.length; ++i)
        {
            Log.d(LOG_TAG, "patchTag oldTechList[" + i + "] = " + oldTechList[i]);
        }

        Bundle[] oldTechExtras = oldParcel.createTypedArray(Bundle.CREATOR);
        for (int i = 0; i < oldTechExtras.length; ++i)
        {
            Log.d(LOG_TAG, "patchTag oldTechExtras[" + i + "] = "
                    + (oldTechExtras[i] == null ? "null" : oldTechExtras[i].toString()));
        }
        int serviceHandle = oldParcel.readInt();
        int isMock = oldParcel.readInt();
        IBinder tagService;
        if (isMock == 0)
        {
            tagService = oldParcel.readStrongBinder();
        }
        else
        {
            tagService = null;
        }
        oldParcel.recycle();

        int nfcaIdx = -1;
        int mcIdx = -1;
        short sak = 0;
        boolean isFirstSak = true;

        for (int i = 0; i < techList.length; i++)
        {
            if (techList[i].equals(NfcA.class.getName()))
            {
                if (nfcaIdx == -1)
                {
                    nfcaIdx = i;
                }
                if (oldTechExtras[i] != null && oldTechExtras[i].containsKey("sak"))
                {
                    sak = (short) (sak | oldTechExtras[i].getShort("sak"));
                    isFirstSak = nfcaIdx == i;
                }
            }
            else if (techList[i].equals(MifareClassic.class.getName()))
            {
                mcIdx = i;
            }
        }

        boolean modified = false;

        // Patch the double NfcA issue (with different SAK) for
        // Sony Z3 devices.
        if (!isFirstSak)
        {
            oldTechExtras[nfcaIdx].putShort("sak", sak);
            modified = true;
        }

        // Patch the wrong index issue for HTC One devices.
        if (nfcaIdx != -1 && mcIdx != -1 && oldTechExtras[mcIdx] == null)
        {
            oldTechExtras[mcIdx] = oldTechExtras[nfcaIdx];
            modified = true;
        }

        if (!modified)
        {
            // Old tag was not modivied. Return the old one.
            return tag;
        }

        // Old tag was modified. Create a new tag with the new data.
        Parcel newParcel = Parcel.obtain();
        newParcel.writeInt(id.length);
        newParcel.writeByteArray(id);
        newParcel.writeInt(oldTechList.length);
        newParcel.writeIntArray(oldTechList);
        newParcel.writeTypedArray(oldTechExtras, 0);
        newParcel.writeInt(serviceHandle);
        newParcel.writeInt(isMock);
        if (isMock == 0)
        {
            newParcel.writeStrongBinder(tagService);
        }
        newParcel.setDataPosition(0);
        Tag newTag = Tag.CREATOR.createFromParcel(newParcel);
        newParcel.recycle();

        return newTag;
    }
}
