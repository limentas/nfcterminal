// android/src/UncaughtExceptionHandler.java
package dev.slebe.nfcterminal;

import android.util.Log;

public class UncaughtExceptionHandler implements Thread.UncaughtExceptionHandler
{
    private static final String LOG_TAG = "NfcTerminalActivity";
    private Thread.UncaughtExceptionHandler oldHandler;

    public UncaughtExceptionHandler()
    {
        // save old exception handler
        oldHandler = Thread.getDefaultUncaughtExceptionHandler();
    }

    @Override
    public void uncaughtException(Thread thread, Throwable throwable)
    {
        Log.e(LOG_TAG,
            "Unhandled exception occured. Thread " + thread.getName() + " id: " + thread.getId(),
            throwable);
        NativeFunctions.unhanledExceptionOccurredNotice(thread.getName(), throwable.toString());
        //call old exception handler
        if(oldHandler != null)
            oldHandler.uncaughtException(thread, throwable);
    }
}
