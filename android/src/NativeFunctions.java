// android/src/NativeFunctions.java
package dev.slebe.nfcterminal;

public class NativeFunctions
{
    public static final int ISO14443_3A = 1;
    public static final int ISO14443_3B = 1<<1;
    public static final int ISO14443_4 = 1<<2;

    public static native void tagDiscoveredNotice(int techs, byte[] id, byte[] atqa, short sak,
                                                  byte[] historicalBytes);
    public static native void backButtonClickedNotice();
    public static native void nfcNotSupportedNotice();
    public static native void nfcEnabledNotice(boolean enabled);
    public static native void tagLostNotice();
    public static native void unhanledExceptionOccurredNotice(String threadInfo, String info);
}
