#include "cardinfo.h"
#include "enums.hpp"

#include <QtQml>

namespace NfcTerminal {

CardInfo::CardInfo():
    m_hasCard(false),
    m_iso14443_3ASupported(false),
    m_iso14443_3BSupported(false),
    m_iso14443_4Supported(false),
    m_maxTranscieveLength(255),
    m_sak(0),
    m_atqa(0)
{
#ifdef USE_STUBS
    m_hasCard = true;
    m_iso14443_3ASupported = true;
    m_iso14443_3BSupported = true;
    m_iso14443_4Supported = true;
    setAtqa(28);
    setSak(32);
    setUid(QByteArrayLiteral("\x01\x02\x03\x04\x05\x06\x07"));
    setHistoricalBytes(QByteArrayLiteral("\x02\x02\x03\x04\x05\x06"));
    setApplicationData(QByteArrayLiteral("\x03\x02\x03\x04\x05"));
    setProtocolInfo(QByteArrayLiteral("\x04\x02\x03\x04"));
    setHighLayerResponse(QByteArrayLiteral("\x05\x02\x03"));
#endif
}

void CardInfo::registerTypes()
{
    qmlRegisterUncreatableType<CardInfo>("NfcTerminal", 1, 0, "CardInfo", "");
}

bool CardInfo::hasCard() const
{
    return m_hasCard;
}

bool CardInfo::getIso14443_3ASupported() const
{
    return m_iso14443_3ASupported;
}

bool CardInfo::getIso14443_3BSupported() const
{
    return m_iso14443_3BSupported;
}

bool CardInfo::getIso14443_4Supported() const
{
    return m_iso14443_4Supported;
}

int CardInfo::getMaxTranscieveLength() const
{
    return m_maxTranscieveLength;
}

uint CardInfo::getSak() const
{
    return m_sak;
}

uint CardInfo::getAtqa() const
{
    return m_atqa;
}

QByteArray CardInfo::getUid() const
{
    return m_uid;
}

QString CardInfo::getUidHex() const
{
    return m_uidHex;
}

QByteArray CardInfo::getHistoricalBytes() const
{
    return m_historicalBytes;
}

QString CardInfo::getHistoricalBytesHex() const
{
    return m_historicalBytesHex;
}

QByteArray CardInfo::getApplicationData() const
{
    return m_applicationData;
}

QString CardInfo::getApplicationDataHex() const
{
    return m_applicationDataHex;
}

QByteArray CardInfo::getProtocolInfo() const
{
    return m_protocolInfo;
}

QString CardInfo::getProtocolInfoHex() const
{
    return m_protocolInfoHex;
}

QByteArray CardInfo::getHighLayerResponse() const
{
    return m_highLayerResponse;
}

QString CardInfo::getHighLayerResponseHex() const
{
    return m_highLayerResponseHex;
}

void CardInfo::setHasCard(bool value)
{
    if (m_hasCard == value)
        return;
    m_hasCard = value;
    emit hasCardChanged(m_hasCard);
}

void CardInfo::setMaxTranscieveLength(int length)
{
    if (m_maxTranscieveLength == length)
        return;
    m_maxTranscieveLength = length;
    emit maxTranscieveLengthChanged(m_maxTranscieveLength);
}

void CardInfo::setTagTechnologies(int newTechs)
{
    auto newIso14443_3ASupported = newTechs & TagTechnologies::ISO14443_3A;
    if (newIso14443_3ASupported != m_iso14443_3ASupported)
    {
        m_iso14443_3ASupported = newIso14443_3ASupported;
        emit iso14443_3ASupportedChanged(m_iso14443_3ASupported);
    }

    auto newIso14443_3BSupported = newTechs & TagTechnologies::ISO14443_3B;
    if (newIso14443_3BSupported != m_iso14443_3BSupported)
    {
        m_iso14443_3BSupported = newIso14443_3BSupported;
        emit iso14443_3BSupportedChanged(m_iso14443_3BSupported);
    }

    auto newIso14443_4Supported = newTechs & TagTechnologies::ISO14443_4;
    if (newIso14443_4Supported != m_iso14443_4Supported)
    {
        m_iso14443_4Supported = newIso14443_4Supported;
        emit iso14443_4SupportedChanged(m_iso14443_4Supported);
    }
}

void CardInfo::setSak(uint newSak)
{
    if (m_sak == newSak)
        return;

    m_sak = newSak;
    emit sakChanged(m_sak);
}

void CardInfo::setAtqa(uint newAtqa)
{
    if (m_atqa == newAtqa)
        return;

    m_atqa = newAtqa;
    emit atqaChanged(m_atqa);
}

void CardInfo::setUid(const QByteArray &newUid)
{
    if (m_uid == newUid)
        return;

    m_uid = newUid;
    m_uidHex = QString::fromLatin1(m_uid.toHex(' '));
    emit uidChanged(m_uid);
    emit uidHexChanged(m_uidHex);
}

void CardInfo::setHistoricalBytes(const QByteArray &newHb)
{
    if (m_historicalBytes == newHb)
        return;

    m_historicalBytes = newHb;
    m_historicalBytesHex = QString::fromLatin1(m_historicalBytes.toHex(' '));
    emit historicalBytesChanged(m_historicalBytes);
    emit historicalBytesHexChanged(m_historicalBytesHex);
}

void CardInfo::setApplicationData(const QByteArray &appData)
{
    if (m_applicationData == appData)
        return;

    m_applicationData = appData;
    m_applicationDataHex = QString::fromLatin1(m_applicationData.toHex(' '));
    emit applicationDataChanged(m_applicationData);
    emit applicationDataHexChanged(m_applicationDataHex);
}

void CardInfo::setProtocolInfo(const QByteArray &protocolInfo)
{
    if (m_protocolInfo == protocolInfo)
        return;

    m_protocolInfo = protocolInfo;
    m_protocolInfoHex = QString::fromLatin1(m_protocolInfo.toHex(' '));
    emit protocolInfoChanged(m_protocolInfo);
    emit protocolInfoHexChanged(m_protocolInfoHex);
}

void CardInfo::setHighLayerResponse(const QByteArray &highLayerResp)
{
    if (m_highLayerResponse == highLayerResp)
        return;

    m_highLayerResponse = highLayerResp;
    m_highLayerResponseHex = QString::fromLatin1(m_highLayerResponse.toHex(' '));
    emit highLayerResponseChanged(m_highLayerResponse);
    emit highLayerResponseHexChanged(m_highLayerResponseHex);
}

} // NfcTerminal namespace

