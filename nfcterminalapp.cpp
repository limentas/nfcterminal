#include "nfcterminalapp.h"
#include "jnimethods.h"
#include "javanfcreader.h"
#include "exceptions.hpp"

#include <CommonLib/autodeleter.hpp>
#include <CommonLib/hexutils.h>

#include <QTimer>
#include <QSysInfo>
#include <QOperatingSystemVersion>
#include <QtQml>
#include <QString>

#define APP_VERSION_MAJOR   1
#define APP_VERSION_MINOR   1
#define APP_VERSION_BUILD   3
#define APP_RELEASE_DATE    "2019-09-09"

using namespace CommonLib;

namespace NfcTerminal {

NfcTerminalApp *NfcTerminalApp::m_instance = nullptr;

void NfcTerminalApp::init(ILogger &logger, ISettings &settings)
{
    static NfcTerminalApp app(logger, settings);
    m_instance = &app;
    m_instance->initPrivate();
}

void NfcTerminalApp::registerModels(QQmlContext *context)
{
    if (instance())
        instance()->registerModelsPrivate(context);
}

NfcTerminalApp *NfcTerminalApp::instance()
{
    return m_instance;
}

void NfcTerminalApp::tagDiscoveredNotice(int techs, const QByteArray &id, const QByteArray &atqa,
                                         int sak, const QByteArray &historicalBytes)
{
    if (instance())
        instance()->tagDiscovered(techs, id, atqa, sak, historicalBytes);
}

void NfcTerminalApp::backButtonClickedNotice()
{
    if (instance())
        instance()->backButtonClickedPrivate();
}

void NfcTerminalApp::nfcNotSupportedNotice()
{
    if (instance())
        instance()->nfcNotSupportedPrivate();
}

void NfcTerminalApp::nfcEnabledNotice(bool enabled)
{
    if (instance())
        instance()->nfcEnabledPrivate(enabled);
}

void NfcTerminalApp::tagLostNotice()
{
    if (instance())
        instance()->tagLostNoticePrivate();
}

void NfcTerminalApp::unhandledExceptionOccurred(const QString &source, const QString &threadInfo,
                                                const QString &info)
{
    if (instance())
        instance()->unhandledExceptionOccurredPrivate(source, threadInfo, info);
}

void NfcTerminalApp::logMsg(const QString &msg)
{
    if (instance())
        instance()->logMsgSlot(msg);
}

void NfcTerminalApp::logExchange(ExchangeDirection direction, const QDateTime &time,
                                 const QByteArray &data)
{
    if (instance())
        instance()->logExchangePrivate(direction, time, data);
}

void NfcTerminalApp::logExchange(ExchangeDirection direction, TagTechnologies tech, const QDateTime &time,
                                 const QByteArray &data)
{
    if (instance())
        instance()->logExchangePrivate(direction, tech, time, data);
}

void NfcTerminalApp::logResponseError(const QDateTime &time, ResponseError error)
{
    if (instance())
        instance()->logResponseErrorPrivate(time, error);
}

bool NfcTerminalApp::nfcSupported() const
{
    return m_nfcSupported;
}

bool NfcTerminalApp::nfcEnabled() const
{
    return m_nfcEnabled;
}

bool NfcTerminalApp::getAppIsBusy() const
{
    return m_appIsBusy;
}

int NfcTerminalApp::getBannerHeight() const
{
    return m_bannerHeight;
}

bool NfcTerminalApp::getForceTurnOnInternet() const
{
    return m_forceTurnOnInternet;
}

QString NfcTerminalApp::getAppVersion() const
{
    return m_appVersion.toString();
}

QDate NfcTerminalApp::getAppReleaseDate() const
{
    return m_appReleaseDate;
}

ExchangeLogModel *NfcTerminalApp::getExchangeLog() const
{
    return m_exchangeLog;
}

QString NfcTerminalApp::getExchangeLogString() const
{
    return m_exchangeLogString;
}

FavoriteCommandsModel *NfcTerminalApp::getFavoriteCommands() const
{
    return m_favoriteCommands;
}

CardInfo *NfcTerminalApp::getCardInfo() const
{
    return m_cardInfo;
}

bool NfcTerminalApp::isDemoVersion() const
{
#ifdef DEMO_VERSION
    return true;
#else
    return false;
#endif
}

NfcTerminalApp::NfcTerminalApp(ILogger &logger, ISettings &settings) :
    QObject(nullptr),
    m_logger(logger),
    m_settings(settings),
    m_reader(new JavaNfcReader),
    m_appVersion(APP_VERSION_MAJOR, APP_VERSION_MINOR, APP_VERSION_BUILD),
    m_appReleaseDate(QDate::fromString(APP_RELEASE_DATE, Qt::ISODate)),
    m_exchangeLog(new ExchangeLogModel),
    m_cardInfo(new CardInfo)
{
#ifdef USE_STUBS
    m_nfcSupported = true;
#else
    m_nfcSupported = JniMethods::isNfcSupported();
#endif
    m_nfcEnabled = JniMethods::isNfcEnabled();
    auto appInstallTime = JniMethods::getAppInstallTime();
    logMsg(QString("AppInstallTime = %1").arg(appInstallTime.toString(Qt::ISODate)));
    if (!m_settings.value("LastAdTime", m_lastLoadedAdTime))
    {
        m_lastLoadedAdTime = appInstallTime;
    }

    m_forceTurnOnInternet = m_lastLoadedAdTime.addSecs(60) < QDateTime::currentDateTime();

    m_logger.logMsg(QString("LastAdTime = %1").arg(m_lastLoadedAdTime.toString(Qt::ISODate)));
}

NfcTerminalApp::~NfcTerminalApp()
{
    delete m_favoriteCommands;
    delete m_cardInfo;
    delete m_exchangeLog;
    delete m_reader;
}

void NfcTerminalApp::initPrivate()
{
    m_favoriteCommands = new FavoriteCommandsModel();
    QTimer::singleShot(0, this, &NfcTerminalApp::requestTagsOnStartup);
}

void NfcTerminalApp::registerModelsPrivate(QQmlContext *context)
{
    qmlRegisterUncreatableMetaObject(NfcTerminal::staticMetaObject, "NfcTerminal", 1, 0,
                                     "ExchangeDirection", "");
    qmlRegisterUncreatableMetaObject(NfcTerminal::staticMetaObject, "NfcTerminal", 1, 0,
                                     "TagTechnologies", "");
    ExchangeLogModel::registerTypes();
    FavoriteCommandsModel::registerTypes();
    CardInfo::registerTypes();
    context->setContextProperty("nfcTerminalApp", this);
}

void NfcTerminalApp::tagDiscovered(int techs, const QByteArray &id, const QByteArray &atqa, int sak,
                                    const QByteArray &historicalBytes)
{
    setAppIsBusy(true);
    std::function<void()> f = [=]() {
        this->setAppIsBusy(false);
    };
    AutoDeleter busySwitcher(f); //will switch of busy indicator at any return

    auto idHex = id.toHex();
    auto atqaHex = atqa.toHex();
    auto hbHex = historicalBytes.toHex();
    m_logger.logMsg(QString("NfcTerminalApp::nfcaDiscovered id=%1  sak=%2  atqa=%3  hb=%4")
                    .arg(QString::fromLatin1(idHex.constData(), idHex.size()))
                    .arg(QString::number(sak))
                    .arg(QString::fromLatin1(atqaHex.constData(), atqaHex.size()))
                    .arg(QString::fromLatin1(hbHex.constData(), hbHex.size())));

    m_cardInfo->setTagTechnologies(techs);
    m_cardInfo->setSak((uint)sak);
    m_cardInfo->setUid(id);
    QDataStream atqaStream(atqa);
    uint16_t atqaNumber;
    atqaStream >> atqaNumber;
    m_cardInfo->setAtqa(atqaNumber);
    m_cardInfo->setHistoricalBytes(historicalBytes);
    m_cardInfo->setHasCard(true);
}

void NfcTerminalApp::logMsgSlot(const QString &msg)
{
    m_logger.logMsg(msg);
}

void NfcTerminalApp::requestEnableNfc()
{
#ifdef USE_STUBS
    nfcEnabledPrivate(true);
    QTimer::singleShot(1000, Qt::CoarseTimer, this, SLOT(applyTestTicket()));
#else
    JniMethods::requestEnableNfc();
#endif
}

void NfcTerminalApp::hideAdBanner()
{
#ifdef ENABLE_ADS
    AppodealAds::hide(AppodealAds::BANNER);
    m_bannerHided = true;
#endif
}

void NfcTerminalApp::restoreAdBanner()
{
    if (!m_bannerHided)
        return;
    m_bannerHided = false;
    //AppodealAds::show(AppodealAds::BANNER);
}

void NfcTerminalApp::sendFeedback(const QString &returnAddress, const QString &name,
                                  const QString &message)
{
    if (sendFeedbackPrivate(returnAddress, name, QString::fromUtf8("NfcTerminal feedback"), message))
    {
        emitErrorOccured(tr("The message was sent successfully. Thank you!"));
    }
    else
    {
        emitErrorOccured(tr("Connection error. Please check your connection and try again."));
    }
}

void NfcTerminalApp::transceiveHex(const QString &hexCmd, int tech)
{
    if (!instance())
        return;
    instance()->transceiveHexPrivate(hexCmd, (TagTechnologies)tech);
}

void NfcTerminalApp::requestTagsOnStartup()
{
    JniMethods::requestTagsOnStartup();
}

void NfcTerminalApp::backButtonClickedPrivate()
{
    emit backButtonClicked();
}

void NfcTerminalApp::nfcNotSupportedPrivate()
{
    if (m_nfcSupported)
    {
        m_nfcSupported = false;
        emit nfcSupportedChanged(m_nfcSupported);
    }
}

void NfcTerminalApp::nfcEnabledPrivate(bool enabled)
{
    m_logger.logMsg(QString("nfcEnabledPrivate %1").arg(enabled));
    if (m_nfcEnabled != enabled)
    {
        m_nfcEnabled = enabled;
        emit nfcEnabledChanged(m_nfcEnabled);
    }
}

void NfcTerminalApp::tagLostNoticePrivate()
{
    m_cardInfo->setHasCard(false);
    m_cardInfo->setTagTechnologies(0);
    m_cardInfo->setSak(0);
    m_cardInfo->setAtqa(0);
    m_cardInfo->setUid(QByteArray());
    m_cardInfo->setHistoricalBytes(QByteArray());
}

void NfcTerminalApp::unhandledExceptionOccurredPrivate(const QString &source,
                                                       const QString &threadInfo,
                                                       const QString &info)
{
    sendFeedbackPrivate(QString::fromUtf8("do-not-reply"),
                        source,
                        QString::fromUtf8("NfcTerminal autoreport"),
                        QString::fromUtf8("Thread: %1 info: %2\n").arg(threadInfo).arg(info));
}

void NfcTerminalApp::setAppIsBusy(bool busy)
{
    if (m_appIsBusy != busy)
    {
        m_appIsBusy = busy;
        emit appIsBusyChanged(m_appIsBusy);
    }
}

void NfcTerminalApp::setLastAdTime(const QDateTime &time)
{
    if (m_lastLoadedAdTime.addSecs(30) <= time)
    {
        m_lastLoadedAdTime = time;
        m_settings.setValue("LastAdTime", m_lastLoadedAdTime);
        if (m_forceTurnOnInternet)
        {
            m_forceTurnOnInternet = false;
            emit forceTurnOnInternetChanged(m_forceTurnOnInternet);
        }
    }
}

bool NfcTerminalApp::sendFeedbackPrivate(const QString &returnAddress, const QString &name,
                                         const QString &title, const QString &message)
{
    QString resultMessage(QString("From %1 <%2>\n\n").arg(name).arg(returnAddress));
    resultMessage += message;
    m_logger.logMsg(QString("Build ABI: %1").arg(QSysInfo::buildAbi()));
    m_logger.logMsg(QString("Build CPU arch: %1").arg(QSysInfo::buildCpuArchitecture()));
    m_logger.logMsg(QString("CPU arch: %1").arg(QSysInfo::currentCpuArchitecture()));
    m_logger.logMsg(QString("Kernel: %1").arg(QSysInfo::kernelType()));
    m_logger.logMsg(QString("Kernel v: %1").arg(QSysInfo::kernelVersion()));
    m_logger.logMsg(QString("Host: %1").arg(QSysInfo::machineHostName()));
    m_logger.logMsg(QString("Product: %1").arg(QSysInfo::prettyProductName()));
    m_logger.logMsg(QString("Product type: %1").arg(QSysInfo::productType()));
    m_logger.logMsg(QString("Product v: %1").arg(QSysInfo::productVersion()));
    auto osVersion = QOperatingSystemVersion::current();
    m_logger.logMsg(QString("OS: %1 %2.%3.%4").arg(osVersion.name()).arg(osVersion.majorVersion())
                    .arg(osVersion.minorVersion()).arg(osVersion.microVersion()));
    m_logger.logMsg(QString("Brand: %1").arg(JniMethods::getDeviceBrand()));
    m_logger.logMsg(QString("Device: %1").arg(JniMethods::getDeviceName()));
    m_logger.logMsg(QString("Fingerprint: %1").arg(JniMethods::getDeviceFingerprint()));
    m_logger.logMsg(QString("Hardware: %1").arg(JniMethods::getDeviceHardware()));
    m_logger.logMsg(QString("Model: %1").arg(JniMethods::getDeviceModel()));
    m_logger.logMsg(QString("Build Product: %1").arg(JniMethods::getDeviceProduct()));

    resultMessage += QString("\n\n\n-------------------------------\n"
                             "Tech info: \n"
                             "Brand: %1\n"
                             "Device: %2\n"
                             "Fingerprint: %3\n"
                             "Hardware: %4\n"
                             "Model: %5\n"
                             "Build Product: %6\n"
                             "OS: %7"
                             "Brand: %8"
                             "Model: %9")
                     .arg(JniMethods::getDeviceBrand())
                     .arg(JniMethods::getDeviceName())
                     .arg(JniMethods::getDeviceFingerprint())
                     .arg(JniMethods::getDeviceHardware())
                     .arg(JniMethods::getDeviceModel())
                     .arg(JniMethods::getDeviceProduct())
                     .arg(QSysInfo::prettyProductName())
                     .arg(JniMethods::getDeviceBrand())
                     .arg(JniMethods::getDeviceModel());

    auto logsData = JniMethods::takeLogs();
    bool compressed = false;
    if (logsData.size() > 19900000) //20 Mb maximum
    {
        logsData = qCompress(logsData);
        compressed = true;
    }
    return true;
}

void NfcTerminalApp::adLoadedPrivate(int height)
{
    m_nextBannerHeight = height;
}

void NfcTerminalApp::adShownPrivate()
{
    setLastAdTime(QDateTime::currentDateTime());
    m_bannerHeight = m_nextBannerHeight;
    emit bannerHeightChanged(m_bannerHeight);
}

void NfcTerminalApp::emitErrorOccured(const QString &msg)
{
    m_logger.logMsg(msg);
    emit errorOccured(msg);
}

void NfcTerminalApp::logExchangePrivate(ExchangeDirection direction, const QDateTime &time,
                                        const QByteArray &data)
{
    m_exchangeLog->addItem(direction, time, data);
    auto line = QString("%1   %2 %3\n")
                .arg(time.toString("hh:mm:ss.zzz"))
                .arg(direction == ExchangeDirection::ToCard ? ">>" : "<<")
                .arg(QString::fromLatin1(data.toHex(' ')));
    logExchangeString(line);
}

void NfcTerminalApp::logExchangePrivate(ExchangeDirection direction, TagTechnologies tech,
                                        const QDateTime &time, const QByteArray &data)
{
    m_exchangeLog->addItem(direction, tech, time, data);
    auto line = QString("%1 %2 %3 %4\n")
                .arg(time.toString("hh:mm:ss.zzz"))
                .arg((int)tech)
                .arg(direction == ExchangeDirection::ToCard ? ">>" : "<<")
                .arg(QString::fromLatin1(data.toHex(' ')));
    logExchangeString(line);
}

void NfcTerminalApp::logResponseErrorPrivate(const QDateTime &time, ResponseError error)
{
    m_exchangeLog->addItem(time, error);
    auto line = QString("%1   %2 %3\n")
                .arg(time.toString("hh:mm:ss.zzz"))
                .arg("<<")
                .arg("error");
    logExchangeString(line);
}

void NfcTerminalApp::logExchangeString(const QString &newLine)
{
    m_exchangeLogString.append(newLine);
    emit exchangeLogStringChanged(m_exchangeLogString);
}

void NfcTerminalApp::transceiveHexPrivate(const QString &hexCmd, TagTechnologies tech)
{
    m_reader->tranceive(HexUtils::hexToBytes(hexCmd), tech);
}

} // NfcTerminal namespace
