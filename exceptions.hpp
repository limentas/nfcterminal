#ifndef EXCEPTIONS_HPP
#define EXCEPTIONS_HPP

#include <QString>

#include <exception>

namespace NfcTerminal {

class BaseException: public std::exception
{
public:
    BaseException(const QString &msg) : m_msg(msg) {}
    virtual ~BaseException() = default;

    virtual const char *what() const Q_DECL_NOEXCEPT override {return m_msg.toLocal8Bit().constData();}

private:
    QString m_msg;
};

class NotSupportedException : public BaseException
{
public:
    NotSupportedException() : BaseException(QT_TR_NOOP("Not supported")) {}
};

class CardLostException : public BaseException
{
public:
    CardLostException() : BaseException(QT_TR_NOOP("Card was lost")) {}
};

class CardReadException : public BaseException
{
public:
    CardReadException() : BaseException(QT_TR_NOOP("Error while card reading")) {}
};

} // NfcTerminal namespace

#endif // EXCEPTIONS_HPP
