#ifndef IREADER_HPP
#define IREADER_HPP

#include "enums.hpp"

class QByteArray;

namespace NfcTerminal {

class IReader
{
public:
    virtual ~IReader() = default;

    virtual QByteArray tranceive(const QByteArray &request, TagTechnologies tech) = 0;
};

} // NfcTerminal namespace

#endif // IREADER_HPP
