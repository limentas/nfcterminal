#ifndef EXCHANGELOGMODEL_H
#define EXCHANGELOGMODEL_H

#include <QAbstractListModel>
#include <QList>
#include <QDateTime>

#include "enums.hpp"
#include "responseerror.hpp"

namespace NfcTerminal {

class ExchangeLogModel: public QAbstractListModel
{
    Q_OBJECT
public:
    ExchangeLogModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    QHash<int, QByteArray> roleNames() const override;

    static void registerTypes();

    void addItem(ExchangeDirection direction, TagTechnologies tech, const QDateTime &time,
                 const QByteArray &data);
    void addItem(ExchangeDirection direction, const QDateTime &time, const QByteArray &data);
    void addItem(const QDateTime &time, ResponseError error);

private:
    static const int DIRECTION_ROLE = Qt::UserRole + 1;
    static const int TIME_ROLE = Qt::UserRole + 2;
    static const int TAG_TECH_ROLE = Qt::UserRole + 3;

private:
    struct LogItem
    {
        ExchangeDirection direction;
        QByteArray  data;
        QDateTime   time;
        bool        wasError;
        ResponseError responseError;
        TagTechnologies tech;
    };

    QList<LogItem>        m_items;
};

} // NfcTerminal namespace

#endif // EXCHANGELOGMODEL_H
