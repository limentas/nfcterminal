#include "nfcterminalapp.h"
#include "jnimethods.h"

#include <functional>

#include <CommonLib/catlogger.h>
#include <CommonLib/appsettings.h>

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QTranslator>
#include <QString>
#include <QIcon>

#ifdef Q_OS_ANDROID
#include <QtSvg>    //Because deployment sometimes just forgets to include this lib otherwise
#endif

using namespace CommonLib;
using namespace NfcTerminal;

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setOrganizationDomain("slebe.dev");
    QCoreApplication::setApplicationName("NfcTerminal");
    QGuiApplication app(argc, argv);
    app.setWindowIcon(QIcon(":/icons/logo.png"));

    //QTranslator translator;
    //if (translator.load(":/translations/tickets_info_ru.qm"))
    //    app.installTranslator(&translator);

    CatLogger logger(&JniMethods::logcat);
    AppSettings settings("NfcTerminal");

    NfcTerminalApp::init(logger, settings);

    logger.logMsg("start");

    QQmlApplicationEngine engine;

    auto context = engine.rootContext();
    context->setContextProperty("logger", &logger);
    NfcTerminalApp::registerModels(context);

    const QUrl url(QStringLiteral("qrc:/qml/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
