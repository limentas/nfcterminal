#ifndef NFCTERMINAL_H
#define NFCTERMINAL_H

#include <CommonLib/ilogger.hpp>
#include <CommonLib/isettings.hpp>
#include "ireader.hpp"
#include "exchangelogmodel.h"
#include "enums.hpp"
#include "responseerror.hpp"
#include "cardinfo.h"
#include "favoritecommandsmodel.h"

#include <QString>
#include <QByteArray>
#include <QObject>
#include <QQmlContext>
#include <QVersionNumber>

using namespace CommonLib;

namespace NfcTerminal {

class NfcTerminalApp : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool nfcSupported READ nfcSupported NOTIFY nfcSupportedChanged)
    Q_PROPERTY(bool nfcEnabled READ nfcEnabled NOTIFY nfcEnabledChanged)
    Q_PROPERTY(bool appIsBusy READ getAppIsBusy NOTIFY appIsBusyChanged)
    Q_PROPERTY(int bannerHeight READ getBannerHeight NOTIFY bannerHeightChanged)
    Q_PROPERTY(bool forceTurnOnInternet READ getForceTurnOnInternet
               NOTIFY forceTurnOnInternetChanged)
    Q_PROPERTY(QString appVersion READ getAppVersion CONSTANT)
    Q_PROPERTY(QDate appReleaseDate READ getAppReleaseDate CONSTANT)
    Q_PROPERTY(ExchangeLogModel *exchangeLog READ getExchangeLog NOTIFY exchangeLogChanged)
    Q_PROPERTY(QString exchangeLogString READ getExchangeLogString NOTIFY exchangeLogStringChanged)
    Q_PROPERTY(FavoriteCommandsModel *favoriteCommands READ getFavoriteCommands NOTIFY favoriteCommandsChanged)
    Q_PROPERTY(CardInfo *cardInfo READ getCardInfo NOTIFY cardInfoChanged)
    Q_PROPERTY(bool isDemoVersion READ isDemoVersion CONSTANT)

public:
    static void init(ILogger &logger, ISettings &settings);
    static void registerModels(QQmlContext *context);

    static NfcTerminalApp *instance();

public:
    static void tagDiscoveredNotice(int techs, const QByteArray &id, const QByteArray &atqa, int sak,
                                    const QByteArray &historicalBytes);
    static void backButtonClickedNotice(); //this method called from java activity
    static void nfcNotSupportedNotice(); //this method called from java activity
    static void nfcEnabledNotice(bool enabled); //this method called from java activity
    static void tagLostNotice(); //this method called from java activity
    //this method called from java activity
    static void unhandledExceptionOccurred(const QString &source, const QString &threadInfo,
                                           const QString &info);
    static void logMsg(const QString &msg);

    static void logExchange(ExchangeDirection direction, const QDateTime &time, const QByteArray &data);
    static void logExchange(ExchangeDirection direction, TagTechnologies tech, const QDateTime &time,
                            const QByteArray &data);
    static void logResponseError(const QDateTime &time, ResponseError error);

public:
    bool nfcSupported() const;
    bool nfcEnabled() const;
    bool getAppIsBusy() const;
    int getBannerHeight() const;
    bool getForceTurnOnInternet() const;
    QString getAppVersion() const;
    QDate getAppReleaseDate() const;
    ExchangeLogModel * getExchangeLog() const;
    QString getExchangeLogString() const;
    FavoriteCommandsModel * getFavoriteCommands() const;
    CardInfo * getCardInfo() const;
    bool isDemoVersion() const;

signals:
    void backButtonClicked();
    void nfcSupportedChanged(bool nfcSupported);
    void nfcEnabledChanged(bool nfcEnabled);
    void appIsBusyChanged(bool appIsBusy);
    void errorOccured(const QString &msg);
    void bannerHeightChanged(int bannerHeight);
    void forceTurnOnInternetChanged(bool forceTurnOnInternet);
    void feedbackBlockedChanged(bool feedbackBlocked);
    void exchangeLogChanged(ExchangeLogModel * exchangeLog);
    void exchangeLogStringChanged(const QString &exchangeLogString);
    void favoriteCommandsChanged(FavoriteCommandsModel * favoriteCommands);
    void cardInfoChanged(CardInfo * cardInfo);

public slots:
    void tagDiscovered(int techs, const QByteArray &id, const QByteArray &atqa, int sak,
                       const QByteArray &historicalBytes);
    void logMsgSlot(const QString &msg);
    void requestEnableNfc();
    void hideAdBanner();
    void restoreAdBanner();
    void sendFeedback(const QString &returnAddress, const QString &name, const QString &message);
    void transceiveHex(const QString &hexCmd, int tech);

private slots:
    void requestTagsOnStartup();

protected:
    explicit NfcTerminalApp(ILogger &logger, ISettings &settings);
    NfcTerminalApp(const NfcTerminalApp &) = delete;
    ~NfcTerminalApp();

    void initPrivate();
    void registerModelsPrivate(QQmlContext *context);

    void backButtonClickedPrivate();
    void nfcNotSupportedPrivate();
    void nfcEnabledPrivate(bool enabled);
    void tagLostNoticePrivate();
    void unhandledExceptionOccurredPrivate(const QString &source, const QString &threadInfo, const QString &info);
    void setAppIsBusy(bool busy);
    void setLastAdTime(const QDateTime &time);
    bool sendFeedbackPrivate(const QString &returnAddress, const QString &name, const QString &title,
                             const QString &message);
    void adLoadedPrivate(int height);
    void adShownPrivate();
    void emitErrorOccured(const QString &msg);
    void logExchangePrivate(ExchangeDirection direction, const QDateTime &time, const QByteArray &data);
    void logExchangePrivate(ExchangeDirection direction, TagTechnologies tech, const QDateTime &time,
                            const QByteArray &data);
    void logResponseErrorPrivate(const QDateTime &time, ResponseError error);
    void logExchangeString(const QString &newLine);
    void transceiveHexPrivate(const QString &hexCmd, NfcTerminal::TagTechnologies tech);

private:
    static const int maxDaysWithoutAds = 14;
    static const int minutesBetweenFeebdack = 60;

    static NfcTerminalApp   * m_instance;
    ILogger                 & m_logger;
    ISettings               & m_settings;
    IReader                 * m_reader;

    bool                      m_nfcSupported = true;
    bool                      m_nfcEnabled = true;
    bool                      m_appIsBusy = false;
    bool                      m_forceTurnOnInternet;
    volatile bool             m_bannerHided = false;

    QVersionNumber            m_appVersion;
    QDate                     m_appReleaseDate;

    QDateTime                 m_lastLoadedAdTime;

    int                       m_bannerHeight = 0;
    int                       m_nextBannerHeight = 120;

    ExchangeLogModel        * m_exchangeLog;
    FavoriteCommandsModel   * m_favoriteCommands;
    CardInfo                * m_cardInfo;
    QString                   m_exchangeLogString;
};

} // NfcTerminal namespace

#endif // NFCTERMINAL_H
