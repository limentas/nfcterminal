#ifndef EXCHANGEDIRECTION_HPP
#define EXCHANGEDIRECTION_HPP

#include <QObject>
#include <QMetaEnum>

namespace NfcTerminal {

Q_NAMESPACE

enum ExchangeDirection
{
    FromCard,
    ToCard
};

Q_ENUM_NS(ExchangeDirection)

enum TagTechnologies
{
    ISO14443_3A = 1,
    ISO14443_3B = 1 << 1,
    ISO14443_4 = 1 << 2
};

Q_FLAG_NS(TagTechnologies)

template<typename QEnum>
QString QtEnumToString(const QEnum value)
{
    return QMetaEnum::fromType<QEnum>().valueToKey(value);
}

} // NfcTerminal namespace

#endif // EXCHANGEDIRECTION_HPP
