#ifndef FAVORITECOMMANDSMODEL_H
#define FAVORITECOMMANDSMODEL_H

#include <QAbstractListModel>

namespace NfcTerminal {

class FavoriteCommandsModel: public QAbstractListModel
{
    Q_OBJECT

public:
    FavoriteCommandsModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    QHash<int, QByteArray> roleNames() const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    static void registerTypes();

    Q_INVOKABLE void addItem(const QByteArray &hexCmd, const QString &name = QString());
    Q_INVOKABLE void removeItem(int index);
    Q_INVOKABLE void removeItem(const QByteArray &hexCmd);
    Q_INVOKABLE bool isInFavorites(const QByteArray &hexCmd);
    Q_INVOKABLE void editItem(int index, const QString &newName);

private:
    bool loadItemsFromStorage();
    void updateStorage();

private:
    static const int NAME_ROLE = Qt::UserRole + 1;
    static const int CMD_ROLE = Qt::UserRole + 2;

private:
    struct FavoriteCommandItem
    {
        QByteArray  cmd;
        QString     name;
    };

    QList<FavoriteCommandItem>        m_items;
};

} // NfcTerminal namespace

#endif // FAVORITECOMMANDSMODEL_H
