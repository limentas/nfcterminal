#ifndef CARDINFO_H
#define CARDINFO_H

#include <QObject>

namespace NfcTerminal {

class CardInfo : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool hasCard READ hasCard NOTIFY hasCardChanged)
    Q_PROPERTY(bool iso14443_3ASupported READ getIso14443_3ASupported NOTIFY iso14443_3ASupportedChanged)
    Q_PROPERTY(bool iso14443_3BSupported READ getIso14443_3BSupported NOTIFY iso14443_3BSupportedChanged)
    Q_PROPERTY(bool iso14443_4Supported READ getIso14443_4Supported NOTIFY iso14443_4SupportedChanged)
    Q_PROPERTY(int maxTranscieveLength READ getMaxTranscieveLength NOTIFY maxTranscieveLengthChanged)
    Q_PROPERTY(uint sak READ getSak NOTIFY sakChanged)
    Q_PROPERTY(uint atqa READ getAtqa NOTIFY atqaChanged)
    Q_PROPERTY(QByteArray uid READ getUid NOTIFY uidChanged)
    Q_PROPERTY(QString uidHex READ getUidHex NOTIFY uidHexChanged)
    Q_PROPERTY(QByteArray historicalBytes READ getHistoricalBytes NOTIFY historicalBytesChanged)
    Q_PROPERTY(QString historicalBytesHex READ getHistoricalBytesHex NOTIFY historicalBytesHexChanged)
    Q_PROPERTY(QByteArray applicationData READ getApplicationData NOTIFY applicationDataChanged)
    Q_PROPERTY(QString applicationDataHex READ getApplicationDataHex NOTIFY applicationDataHexChanged)
    Q_PROPERTY(QByteArray protocolInfo READ getProtocolInfo NOTIFY protocolInfoChanged)
    Q_PROPERTY(QString protocolInfoHex READ getProtocolInfoHex NOTIFY protocolInfoHexChanged)
    Q_PROPERTY(QByteArray highLayerResponse READ getHighLayerResponse NOTIFY highLayerResponseChanged)
    Q_PROPERTY(QString highLayerResponseHex READ getHighLayerResponseHex NOTIFY highLayerResponseHexChanged)

public:
    CardInfo();

    static void registerTypes();

    bool hasCard() const;
    bool getIso14443_3ASupported() const;
    bool getIso14443_3BSupported() const;
    bool getIso14443_4Supported() const;
    int getMaxTranscieveLength() const;
    uint getSak() const;
    uint getAtqa() const;
    QByteArray getUid() const;
    QString getUidHex() const;
    QByteArray getHistoricalBytes() const;
    QString getHistoricalBytesHex() const;
    QByteArray getApplicationData() const;
    QString getApplicationDataHex() const;
    QByteArray getProtocolInfo() const;
    QString getProtocolInfoHex() const;
    QByteArray getHighLayerResponse() const;
    QString getHighLayerResponseHex() const;

public:
    void setHasCard(bool value);
    void setMaxTranscieveLength(int length);
    void setTagTechnologies(int newTechs);
    void setSak(uint newSak);
    void setAtqa(uint newAtqa);
    void setUid(const QByteArray &newUid);
    void setHistoricalBytes(const QByteArray &newHb);
    void setApplicationData(const QByteArray &appData);
    void setProtocolInfo(const QByteArray &protocolInfo);
    void setHighLayerResponse(const QByteArray &higheLayerResp);

signals:
    void hasCardChanged(bool hasCard);
    void iso14443_3ASupportedChanged(bool iso14443_3ASupported);
    void iso14443_3BSupportedChanged(bool iso14443_3BSupported);
    void iso14443_4SupportedChanged(bool iso14443_4Supported);
    void maxTranscieveLengthChanged(int maxTranscieveLength);
    void sakChanged(uint sak);
    void atqaChanged(uint atqa);
    void uidChanged(const QByteArray &uid);
    void uidHexChanged(const QString &uidHex);
    void historicalBytesChanged(const QByteArray &historicalBytes);
    void historicalBytesHexChanged(const QString &historicalBytesHex);
    void applicationDataChanged(QByteArray applicationData);
    void applicationDataHexChanged(QString applicationDataHex);
    void protocolInfoChanged(QByteArray protocolInfo);
    void protocolInfoHexChanged(QString protocolInfoHex);
    void highLayerResponseChanged(QByteArray highLayerResponse);
    void highLayerResponseHexChanged(QString highLayerResponseHex);

private:
    bool        m_hasCard;
    bool        m_iso14443_3ASupported;
    bool        m_iso14443_3BSupported;
    bool        m_iso14443_4Supported;
    int         m_maxTranscieveLength;
    uint        m_sak;
    uint        m_atqa;
    QByteArray  m_uid;
    QString     m_uidHex;
    QByteArray  m_historicalBytes;
    QString     m_historicalBytesHex;
    QByteArray  m_applicationData;
    QString     m_applicationDataHex;
    QByteArray  m_protocolInfo;
    QString     m_protocolInfoHex;
    QByteArray  m_highLayerResponse;
    QString     m_highLayerResponseHex;
};

} // NfcTerminal namespace

#endif // CARDINFO_H
