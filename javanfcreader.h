#ifndef JAVANFCAREADER_H
#define JAVANFCAREADER_H

#include "ireader.hpp"
#include "enums.hpp"

namespace NfcTerminal {

class JavaNfcReader: public IReader
{
public:
    JavaNfcReader();

    virtual ~JavaNfcReader() = default;

    virtual QByteArray tranceive(const QByteArray &request, TagTechnologies tech) override;
};

} // NfcTerminal namespace

#endif // JAVANFCAREADER_H
