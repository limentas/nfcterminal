#include "exchangelogmodel.h"

#include <QHash>
#include <QtQml>
#include <QByteArray>

namespace NfcTerminal {

const int ExchangeLogModel::DIRECTION_ROLE;
const int ExchangeLogModel::TIME_ROLE;
const int ExchangeLogModel::TAG_TECH_ROLE;

ExchangeLogModel::ExchangeLogModel()
{
    m_items.reserve(100);
}

int ExchangeLogModel::rowCount(const QModelIndex &) const
{
    return m_items.size();
}

QVariant ExchangeLogModel::data(const QModelIndex &index, int role) const
{
    auto item = m_items[index.row()];
    switch(role)
    {
        case DIRECTION_ROLE:
            return QVariant(item.direction);
        case TIME_ROLE:
            return QVariant(item.time);
        case TAG_TECH_ROLE:
            return QVariant(item.tech);
        default:
            if (item.wasError)
            {
                switch (item.responseError)
                {
                    case ResponseError::CardLost:
                        return QVariant("timeout");
                    case ResponseError::OtherError:
                        return QVariant("transmission error");
                }
            }
            return QVariant(QString::fromLatin1(item.data.toHex(' ')));
    }
}

QHash<int, QByteArray> ExchangeLogModel::roleNames() const
{
    return *(new QHash<int, QByteArray>(
    {
        {Qt::DisplayRole, "display"},
        {DIRECTION_ROLE, "direction"},
        {TIME_ROLE, "time"},
        {TAG_TECH_ROLE, "technology"}
    }));
}

void ExchangeLogModel::registerTypes()
{
    qmlRegisterUncreatableType<ExchangeLogModel>("NfcTerminal", 1, 0, "ExchangeLogModel", "");
}

void ExchangeLogModel::addItem(ExchangeDirection direction, TagTechnologies tech, const QDateTime &time, const QByteArray &data)
{
    beginInsertRows(QModelIndex(), m_items.size(), m_items.size());
    m_items.push_back(LogItem{direction, data, time, false, ResponseError::OtherError, tech});
    endInsertRows();
}

void ExchangeLogModel::addItem(ExchangeDirection direction, const QDateTime &time, const QByteArray &data)
{
    beginInsertRows(QModelIndex(), m_items.size(), m_items.size());
    m_items.push_back(LogItem{direction, data, time, false, ResponseError::OtherError,
                              TagTechnologies::ISO14443_3A});
    endInsertRows();
}

void ExchangeLogModel::addItem(const QDateTime &time, ResponseError error)
{
    beginInsertRows(QModelIndex(), m_items.size(), m_items.size());
    m_items.push_back(LogItem{FromCard, QByteArray(), time, true, error,
                              TagTechnologies::ISO14443_3A});
    endInsertRows();
}

} // NfcTerminal namespace
