#include "favoritecommandsmodel.h"
#include "nfcterminalapp.h"

#include <QAbstractListModel>
#include <QList>
#include <QtQml>
#include <QByteArray>
#include <QSettings>

namespace NfcTerminal {

const int FavoriteCommandsModel::NAME_ROLE;
const int FavoriteCommandsModel::CMD_ROLE;

FavoriteCommandsModel::FavoriteCommandsModel()
{
    m_items.reserve(20);
    if (!loadItemsFromStorage())
    {
        NfcTerminalApp::logMsg("Seed settings with default commands");
        m_items.push_back(
                    FavoriteCommandItem({QByteArrayLiteral("\x00\xA4\x04\00\x07\xA0\x00\x00\x00\x04\x10\x10\x00"),
                                         QString("Select PayPass AID")}));
        m_items.push_back(
                    FavoriteCommandItem({QByteArrayLiteral("\x80\x00\x00\00\x7F"),
                                         QString("Get MULTOS Data")}));
        m_items.push_back(
                    FavoriteCommandItem({QByteArrayLiteral("\x80\x02\x00\00\x16"),
                                         QString("Get MULTOS manufacturer data")}));
        m_items.push_back(
                    FavoriteCommandItem({QByteArrayLiteral("\x00\xA4\x04\x00\x0E\x31\x50\x41\x59\x2E\x53\x59\x53\x2E\x44\x44\x46\x30\x31\x00"),
                                         QString("Select PSE")}));
        m_items.push_back(
                    FavoriteCommandItem({QByteArrayLiteral("\x00\xA4\x04\x00\x0E\x32\x50\x41\x59\x2E\x53\x59\x53\x2E\x44\x44\x46\x30\x31\x00"),
                                         QString("Select PPSE")}));
        updateStorage();
    }
}

int FavoriteCommandsModel::rowCount(const QModelIndex &) const
{
    return m_items.size();
}

QVariant FavoriteCommandsModel::data(const QModelIndex &index, int role) const
{
    auto item = m_items[index.row()];
    switch(role)
    {
        case NAME_ROLE:
            return QVariant(item.name);
        case CMD_ROLE:
            return QVariant(item.cmd.toHex(' '));
        default:
            return QVariant(item.cmd.toHex(' '));
    }
}

QHash<int, QByteArray> FavoriteCommandsModel::roleNames() const
{
    return *(new QHash<int, QByteArray>(
    {
        {Qt::DisplayRole, "display"},
        {NAME_ROLE, "name"},
        {CMD_ROLE, "cmd"}
                 }));
}

Qt::ItemFlags FavoriteCommandsModel::flags(const QModelIndex &/*index*/) const
{
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
}

bool FavoriteCommandsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role != NAME_ROLE)
        return false;
    editItem(index.row(), value.toString());
    return true;
}

void FavoriteCommandsModel::registerTypes()
{
    qmlRegisterUncreatableType<FavoriteCommandsModel>("NfcTerminal", 1, 0, "FavoriteCommandsModel", "");
}

void FavoriteCommandsModel::addItem(const QByteArray &hexCmd, const QString &name)
{
    auto cmd = QByteArray::fromHex(hexCmd);
    beginInsertRows(QModelIndex(), m_items.size(), m_items.size());
    m_items.push_back(FavoriteCommandItem{cmd, name});
    endInsertRows();
    updateStorage();
}

void FavoriteCommandsModel::removeItem(int index)
{
    beginRemoveRows(QModelIndex(), index, index);
    m_items.removeAt(index);
    endRemoveRows();
    updateStorage();
}

void FavoriteCommandsModel::removeItem(const QByteArray &hexCmd)
{
    int index = -1;
    auto cmd = QByteArray::fromHex(hexCmd);
    for(auto i = 0; i < m_items.size(); ++i)
    {
        if (m_items[i].cmd == cmd)
            index = i;
    }
    if (index >= 0)
        removeItem(index);
}

bool FavoriteCommandsModel::isInFavorites(const QByteArray &hexCmd)
{
    auto cmd = QByteArray::fromHex(hexCmd);
    for(auto item : m_items)
    {
        if (item.cmd == cmd)
            return true;
    }
    return false;
}

void FavoriteCommandsModel::editItem(int index, const QString &newName)
{
    m_items[index].name = newName;
    auto modelIndex = createIndex(index, 0);
    emit dataChanged(modelIndex, modelIndex, QVector<int>{NAME_ROLE});
}

bool FavoriteCommandsModel::loadItemsFromStorage()
{
    m_items.clear();
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, "slebe.dev", "nfc_terminal");
    NfcTerminalApp::logMsg(QString("Settings file: %1").arg(settings.fileName()));
    settings.beginGroup("FavoriteCmds");
    if (!settings.contains("Cmds/size"))
        return false;
    auto count = settings.beginReadArray("Cmds");
    for(auto i = 0; i < count; ++i)
    {
        settings.setArrayIndex(i);
        FavoriteCommandItem item;
        item.name = settings.value("name").toString();
        item.cmd = settings.value("cmd").toByteArray();
        m_items.push_back(item);
    }
    settings.endArray();
    settings.endGroup();
    return true;
}

void FavoriteCommandsModel::updateStorage()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, "slebe.dev", "nfc_terminal");
    settings.beginGroup("FavoriteCmds");
    settings.beginWriteArray("Cmds", m_items.size());
    for(auto i = 0; i < m_items.size(); ++i)
    {
        settings.setArrayIndex(i);
        auto item = m_items[i];
        settings.setValue("name", item.name);
        settings.setValue("cmd", item.cmd);
    }
    settings.endArray();
    settings.endGroup();
    settings.sync();
}

} // NfcTerminal namespace
