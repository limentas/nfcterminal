#include "jnimethods.h"
#include "exceptions.hpp"
#include "nfcterminalapp.h"

#ifndef DO_NOT_USE_ANDROID_EXTRAS
#include <jni.h>
#else
#include <QDebug>
#endif

namespace NfcTerminal {

#ifndef DO_NOT_USE_ANDROID_EXTRAS
jclass JniMethods::m_activityClass = nullptr;
jclass JniMethods::m_throwableClass = nullptr;
jclass JniMethods::m_stackTraceElementClass = nullptr;
jclass JniMethods::m_javaClass = nullptr;
jmethodID JniMethods::m_requestNfcAMethod = nullptr;
jmethodID JniMethods::m_requestNfcBMethod = nullptr;
jmethodID JniMethods::m_requestIsoMethod = nullptr;
jmethodID JniMethods::m_isNfcSupportedMethod = nullptr;
jmethodID JniMethods::m_isNfcEnabledMethod = nullptr;
jmethodID JniMethods::m_requestEnableNfcMethod = nullptr;
jmethodID JniMethods::m_logcatMethod = nullptr;
jmethodID JniMethods::m_takeLogsMethod = nullptr;
jmethodID JniMethods::m_isInternetAvailableMethod = nullptr;
jmethodID JniMethods::m_getAppInstallMethod = nullptr;
jmethodID JniMethods::m_requestTagsOnStartup = nullptr;
jmethodID JniMethods::m_getClassMethod = nullptr;
jmethodID JniMethods::m_getNameMethod = nullptr;
jmethodID JniMethods::m_getMessageMethod = nullptr;
jmethodID JniMethods::m_getStackTraceMethod = nullptr;
jmethodID JniMethods::m_stackTraceToStringMethod = nullptr;
QString *JniMethods::m_deviceBrand = nullptr;
QString *JniMethods::m_deviceName = nullptr;
QString *JniMethods::m_deviceFingerprint = nullptr;
QString *JniMethods::m_deviceHardware = nullptr;
QString *JniMethods::m_deviceModel = nullptr;
QString *JniMethods::m_deviceProduct = nullptr;

QByteArray JniMethods::requestTransceive(const QByteArray &data, TagTechnologies tech)
{
    QAndroidJniEnvironment jniEnv;
    auto dataArray = jniEnv->NewByteArray(data.size());
    jniEnv->SetByteArrayRegion(dataArray, 0, data.size(),
                               reinterpret_cast<const jbyte *>(data.constData()));
    jmethodID methodToCall;
    switch(tech)
    {
        case TagTechnologies::ISO14443_3A:
            methodToCall = m_requestNfcAMethod;
            break;
        case TagTechnologies::ISO14443_3B:
            methodToCall = m_requestNfcBMethod;
            break;
        case TagTechnologies::ISO14443_4:
            methodToCall = m_requestIsoMethod;
            break;
    }

    auto resp = jniEnv->CallStaticObjectMethod(m_activityClass, methodToCall, dataArray);

    jniEnv->DeleteLocalRef(dataArray);

    if (jniEnv->ExceptionCheck())
    {
        auto except = jniEnv->ExceptionOccurred();
        jniEnv->ExceptionClear();
        ExceptionInfo info;
        getExceptionInfo(except, jniEnv, info);
        if (info.exceptionClass == "java.io.IOException")
        {
            throw CardReadException();
        }
        else if (info.exceptionClass == "android.nfc.TagLostException")
        {
            throw CardLostException();
        }
        return QByteArray();
    }

    if (resp == nullptr)
        return QByteArray();

    auto respSize = jniEnv->GetArrayLength((jarray)resp);
    auto respArray = jniEnv->GetByteArrayElements((jbyteArray)resp, nullptr);
    QByteArray ret((const char *)respArray, respSize);
    jniEnv->ReleaseByteArrayElements((jbyteArray)resp, respArray, JNI_ABORT);
    return ret;
}

bool JniMethods::isNfcSupported()
{
    QAndroidJniEnvironment jniEnv;
    auto result = jniEnv->CallStaticBooleanMethod(m_activityClass, m_isNfcSupportedMethod);
    checkExceptions(jniEnv);
    return result;
}

bool JniMethods::isNfcEnabled()
{
    QAndroidJniEnvironment jniEnv;
    auto result = jniEnv->CallStaticBooleanMethod(m_activityClass, m_isNfcEnabledMethod);
    checkExceptions(jniEnv);
    return result;
}

void JniMethods::requestEnableNfc()
{
    QAndroidJniEnvironment jniEnv;
    jniEnv->CallStaticVoidMethod(m_activityClass, m_requestEnableNfcMethod);
    checkExceptions(jniEnv);
}

QString JniMethods::getDeviceBrand()
{
    if (m_deviceBrand)
        return *m_deviceBrand;
    return QString();
}

QString JniMethods::getDeviceName()
{
    if (m_deviceName)
        return *m_deviceName;
    return QString();
}

QString JniMethods::getDeviceFingerprint()
{
    if (m_deviceFingerprint)
        return *m_deviceFingerprint;
    return QString();
}

QString JniMethods::getDeviceHardware()
{
    if (m_deviceHardware)
        return *m_deviceHardware;
    return QString();
}

QString JniMethods::getDeviceModel()
{
    if (m_deviceModel)
        return *m_deviceModel;
    return QString();
}

QString JniMethods::getDeviceProduct()
{
    if (m_deviceProduct)
        return *m_deviceProduct;
    return QString();
}

void JniMethods::logcat(const QString &msg)
{
    QAndroidJniEnvironment jniEnv;
    jniEnv->CallStaticVoidMethod(m_activityClass, m_logcatMethod,
                                 QAndroidJniObject::fromString(msg).object<jstring>());
    checkExceptions(jniEnv);
}

QByteArray JniMethods::takeLogs()
{
    QAndroidJniEnvironment jniEnv;
    auto resp = jniEnv->CallStaticObjectMethod(m_activityClass, m_takeLogsMethod);
    auto logs = QAndroidJniObject(resp).toString();
    checkExceptions(jniEnv);
    return logs.toUtf8();
}

bool JniMethods::isInternetAvailable()
{
    QAndroidJniEnvironment jniEnv;
    auto result = jniEnv->CallStaticBooleanMethod(m_activityClass, m_isInternetAvailableMethod);
    checkExceptions(jniEnv);
    return result;
}

QDateTime JniMethods::getAppInstallTime()
{
    QAndroidJniEnvironment jniEnv;
    auto msFromEpoch = jniEnv->CallStaticLongMethod(m_activityClass, m_getAppInstallMethod);
    checkExceptions(jniEnv);
    return QDateTime::fromMSecsSinceEpoch(msFromEpoch);
}

void JniMethods::requestTagsOnStartup()
{
    QAndroidJniEnvironment jniEnv;
    jniEnv->CallStaticVoidMethod(m_activityClass, m_requestTagsOnStartup);
    checkExceptions(jniEnv);
}

void JniMethods::init(JNIEnv *env)
{
    if (m_activityClass)
        env->DeleteGlobalRef(m_activityClass);
    m_activityClass = reinterpret_cast<jclass>(
                env->NewGlobalRef(env->FindClass("dev/slebe/nfcterminal/NfcTerminalActivity")));
    if (m_throwableClass)
        env->DeleteGlobalRef(m_throwableClass);
    m_throwableClass = reinterpret_cast<jclass>(
                env->NewGlobalRef(env->FindClass("java/lang/Throwable")));
    if (m_stackTraceElementClass)
        env->DeleteGlobalRef(m_stackTraceElementClass);
    m_stackTraceElementClass = reinterpret_cast<jclass>(
                env->NewGlobalRef(env->FindClass("java/lang/StackTraceElement")));
    if (m_javaClass)
        env->DeleteGlobalRef(m_javaClass);
    m_javaClass = reinterpret_cast<jclass>(
                env->NewGlobalRef(env->FindClass("java/lang/Class")));

    m_requestNfcAMethod = env->GetStaticMethodID(m_activityClass,
                                                 "requestNfcA",
                                                 "([B)[B");
    m_requestNfcBMethod = env->GetStaticMethodID(m_activityClass,
                                                 "requestNfcB",
                                                 "([B)[B");
    m_requestIsoMethod = env->GetStaticMethodID(m_activityClass,
                                                 "requestIso",
                                                 "([B)[B");
    m_isNfcSupportedMethod = env->GetStaticMethodID(m_activityClass,
                                                    "isNfcSupported",
                                                    "()Z");
    m_isNfcEnabledMethod = env->GetStaticMethodID(m_activityClass,
                                                  "isNfcEnabled",
                                                  "()Z");
    m_requestEnableNfcMethod = env->GetStaticMethodID(m_activityClass,
                                                      "requestEnableNfc",
                                                      "()V");
    m_logcatMethod = env->GetStaticMethodID(m_activityClass,
                                            "logcat",
                                            "(Ljava/lang/String;)V");
    m_takeLogsMethod = env->GetStaticMethodID(m_activityClass, "takeLogs",
                                              "()Ljava/lang/String;");
    m_isInternetAvailableMethod = env->GetStaticMethodID(m_activityClass, "isInternetAvailable",
                                                         "()Z");
    m_getAppInstallMethod = env->GetStaticMethodID(m_activityClass, "getAppInstallTime", "()J");
    m_requestTagsOnStartup = env->GetStaticMethodID(m_activityClass,
                                                    "requestTagsOnStartup",
                                                    "()V");

    m_getClassMethod = env->GetMethodID(m_throwableClass, "getClass", "()Ljava/lang/Class;");
    m_getNameMethod = env->GetMethodID(m_javaClass, "getName", "()Ljava/lang/String;");
    m_getMessageMethod = env->GetMethodID(m_throwableClass, "getMessage", "()Ljava/lang/String;");
    m_getStackTraceMethod = env->GetMethodID(m_throwableClass, "getStackTrace",
                                             "()[Ljava/lang/StackTraceElement;");
    m_stackTraceToStringMethod =
            env->GetMethodID(m_stackTraceElementClass, "toString", "()Ljava/lang/String;");

    auto androidBuildClass = env->FindClass("android/os/Build");
    if (androidBuildClass)
    {
        auto field = env->GetStaticFieldID(androidBuildClass, "BRAND", "Ljava/lang/String;");
        auto strObject = env->GetStaticObjectField(androidBuildClass, field);
        m_deviceBrand = new QString(QAndroidJniObject(strObject).toString());
        field = env->GetStaticFieldID(androidBuildClass, "DEVICE", "Ljava/lang/String;");
        strObject = env->GetStaticObjectField(androidBuildClass, field);
        m_deviceName = new QString(QAndroidJniObject(strObject).toString());
        field = env->GetStaticFieldID(androidBuildClass, "FINGERPRINT", "Ljava/lang/String;");
        strObject = env->GetStaticObjectField(androidBuildClass, field);
        m_deviceFingerprint = new QString(QAndroidJniObject(strObject).toString());
        field = env->GetStaticFieldID(androidBuildClass, "HARDWARE", "Ljava/lang/String;");
        strObject = env->GetStaticObjectField(androidBuildClass, field);
        m_deviceHardware = new QString(QAndroidJniObject(strObject).toString());
        field = env->GetStaticFieldID(androidBuildClass, "MODEL", "Ljava/lang/String;");
        strObject = env->GetStaticObjectField(androidBuildClass, field);
        m_deviceModel = new QString(QAndroidJniObject(strObject).toString());
        field = env->GetStaticFieldID(androidBuildClass, "PRODUCT", "Ljava/lang/String;");
        strObject = env->GetStaticObjectField(androidBuildClass, field);
        m_deviceProduct = new QString(QAndroidJniObject(strObject).toString());
    }
}

void JniMethods::deinit(JNIEnv *env)
{
    if (m_activityClass)
    {
        env->DeleteGlobalRef(m_activityClass);
        m_activityClass = nullptr;
    }
    if (m_throwableClass)
    {
        env->DeleteGlobalRef(m_throwableClass);
        m_throwableClass = nullptr;
    }
    if (m_javaClass)
    {
        env->DeleteGlobalRef(m_javaClass);
        m_javaClass = nullptr;
    }
    delete m_deviceBrand;
    m_deviceBrand = nullptr;
    delete m_deviceName;
    m_deviceName = nullptr;
    delete m_deviceFingerprint;
    m_deviceFingerprint = nullptr;
    delete m_deviceHardware;
    m_deviceHardware = nullptr;
    delete m_deviceModel;
    m_deviceModel = nullptr;
    delete m_deviceProduct;
    m_deviceProduct = nullptr;
}

void JniMethods::checkExceptions(QAndroidJniEnvironment &env)
{
    if (env->ExceptionCheck())
    {
        auto except = env->ExceptionOccurred();
        env->ExceptionClear();
        ExceptionInfo info;
        getExceptionInfo(except, env, info);
        NfcTerminalApp::unhandledExceptionOccurred(
                        QString::fromUtf8("JNI exception"),
                        QString::fromUtf8("JNI thread"),
                        QString::fromUtf8("%1: %2\n%3")
                            .arg(info.exceptionClass)
                            .arg(info.message)
                            .arg(info.stackTrace));
    }
}

void JniMethods::getExceptionInfo(const jthrowable &except,
                                               QAndroidJniEnvironment &env,
                                               JniMethods::ExceptionInfo &info)
{
    auto classObject = env->CallObjectMethod(except, m_getClassMethod);
    auto className = env->CallObjectMethod(classObject, m_getNameMethod);
    info.exceptionClass = QAndroidJniObject(className).toString();
    auto msgObject = env->CallObjectMethod(except, m_getMessageMethod);
    info.message = QAndroidJniObject(msgObject).toString();
    auto stackTraceArray = env->CallObjectMethod(except, m_getStackTraceMethod);
    auto respSize = env->GetArrayLength((jarray)stackTraceArray);
    for (auto i = 0; i < respSize; ++i)
    {
        auto stElement = env->GetObjectArrayElement((jobjectArray)stackTraceArray, i);
        auto stLine = env->CallObjectMethod(stElement, m_stackTraceToStringMethod);
        info.stackTrace += QString::fromUtf8("\tat %1\n").arg(QAndroidJniObject(stLine).toString());
    }
}

#else

QByteArray JniMethods::requestTransceive(const QByteArray &data, TagTechnologies tech)
{
#ifdef USE_STUBS
    //just append tech type and reverse input
    QByteArray res;
    res.append(1, (char)tech);
    for(auto it = data.crbegin(); it != data.crend(); ++it)
    {
        res.append(1, *it);
    }
    return res;
#else
    throw CardReadException();
#endif
}

bool JniMethods::isNfcSupported()
{
    return true;
}

bool JniMethods::isNfcEnabled()
{
#ifdef USE_STUBS
    return false;
#else
    return false;
#endif
}

void JniMethods::requestEnableNfc()
{

}

void JniMethods::logcat(const QString &msg)
{
    qDebug() << msg;
}

QString JniMethods::getDeviceBrand()
{
    return "Stub Inc";
}

QString JniMethods::getDeviceName()
{
    return "TurboStub+";
}

QString JniMethods::getDeviceFingerprint()
{
    return "";
}

QString JniMethods::getDeviceHardware()
{
    return "PC";
}

QString JniMethods::getDeviceModel()
{
    return "PC";
}

QString JniMethods::getDeviceProduct()
{
    return "Win";
}

QByteArray JniMethods::takeLogs()
{
    return QByteArray();
}

bool JniMethods::isInternetAvailable()
{
    return true;
}

QDateTime JniMethods::getAppInstallTime()
{
    return QDateTime();
}

void JniMethods::requestTagsOnStartup()
{

}

#endif //DO_NOT_USE_ANDROID_EXTRAS

} // NfcTerminal namespace
