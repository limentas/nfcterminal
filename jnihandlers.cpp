#include "nfcterminalapp.h"
#include "jnimethods.h"

#ifndef DO_NOT_USE_ANDROID_EXTRAS
#include <jni.h>
#include <QAndroidJniObject>
#include <QDebug>
//#include "Appodeal/appodealandroid.h"

using namespace NfcTerminal;

extern "C" {

JNIEXPORT void JNICALL tagDiscoveredNotice(JNIEnv *env, jobject /*obj*/, jint techs, jbyteArray id,
                                           jbyteArray atqa, jshort sak, jbyteArray historicalBytes)
{
    QByteArray idByteArray;
    if (id != nullptr)
    {
        auto idSize = env->GetArrayLength(id);
        auto idArray = env->GetByteArrayElements(id, nullptr);
        idByteArray = QByteArray((const char *)idArray, idSize);
        env->ReleaseByteArrayElements(id, idArray, JNI_ABORT);
    }
    QByteArray atqaByteArray;
    if (atqa != nullptr)
    {
        auto atqaSize = env->GetArrayLength(atqa);
        auto atqaArray = env->GetByteArrayElements(atqa, nullptr);
        atqaByteArray = QByteArray((const char *)atqaArray, atqaSize);
        env->ReleaseByteArrayElements(atqa, atqaArray, JNI_ABORT);
    }
    QByteArray hbByteArray;
    if (historicalBytes != nullptr)
    {
        auto hbSize = env->GetArrayLength(historicalBytes);
        auto hbArray = env->GetByteArrayElements(historicalBytes, nullptr);
        hbByteArray = QByteArray((const char *)hbArray, hbSize);
        env->ReleaseByteArrayElements(historicalBytes, hbArray, JNI_ABORT);
    }

    NfcTerminalApp::tagDiscoveredNotice(techs, idByteArray, atqaByteArray, sak, hbByteArray);
}

JNIEXPORT void JNICALL backButtonClickedNotice(JNIEnv * /*env*/, jobject /*obj*/)
{
    NfcTerminalApp::backButtonClickedNotice();
}

JNIEXPORT void JNICALL nfcNotSupportedNotice(JNIEnv * /*env*/, jobject /*obj*/)
{
    NfcTerminalApp::nfcNotSupportedNotice();
}

JNIEXPORT void JNICALL nfcEnabledNotice(JNIEnv * /*env*/, jobject /*obj*/, jboolean enabled)
{
    NfcTerminalApp::nfcEnabledNotice(enabled);
}

JNIEXPORT void JNICALL tagLostNotice(JNIEnv * /*env*/, jobject /*obj*/)
{
    NfcTerminalApp::tagLostNotice();
}

JNIEXPORT void JNICALL unhanledExceptionOccurredNotice(JNIEnv * /*env*/, jobject /*obj*/,
                                                       jstring threadInfo, jstring info)
{
    NfcTerminalApp::unhandledExceptionOccurred(QString::fromUtf8("Pre-mortem java exception"),
                                               QAndroidJniObject(threadInfo).toString(),
                                               QAndroidJniObject(info).toString());
}

}

//create a vector with all our JNINativeMethod(s)
static JNINativeMethod methods[] = {
    {"tagDiscoveredNotice", "(I[B[BS[B)V", reinterpret_cast<void *>(tagDiscoveredNotice)},
    {"backButtonClickedNotice", "()V", reinterpret_cast<void *>(backButtonClickedNotice)},
    {"nfcNotSupportedNotice", "()V", reinterpret_cast<void *>(nfcNotSupportedNotice)},
    {"nfcEnabledNotice", "(Z)V", reinterpret_cast<void *>(nfcEnabledNotice)},
    {"tagLostNotice", "()V", reinterpret_cast<void *>(tagLostNotice)},
    {"unhanledExceptionOccurredNotice", "(Ljava/lang/String;Ljava/lang/String;)V",
        reinterpret_cast<void *>(unhanledExceptionOccurredNotice)}
};

//jint Appodeal_JNI_OnLoad(JavaVM* vm, void*);

// this method is called automatically by Java after the .so file is loaded
JNIEXPORT jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    JNIEnv* env;
    // get the JNIEnv pointer.
    if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK)
        return JNI_ERR;

    // search for Java class which declares the native methods
    jclass javaClass = env->FindClass("dev/slebe/nfcterminal/NativeFunctions");
    if (!javaClass)
        return JNI_ERR;

    // register our native methods
    if (env->RegisterNatives(javaClass, methods,
                             sizeof(methods) / sizeof(methods[0])) < 0)
        return JNI_ERR;

    JniMethods::init(env);
    return JNI_VERSION_1_6;
//    return Appodeal_JNI_OnLoad(vm, reserved);// JNI_VERSION_1_6;
}

JNIEXPORT void JNI_OnUnload(JavaVM* vm, void* /*reserved*/)
{
    JNIEnv* env;
    // get the JNIEnv pointer.
    if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK)
      return ;
    JniMethods::deinit(env);
}

#endif //DO_NOT_USE_ANDROID_EXTRAS
