#ifndef JNIMETHODS_H
#define JNIMETHODS_H

#include "enums.hpp"

#include <QByteArray>
#include <QDateTime>

#ifndef DO_NOT_USE_ANDROID_EXTRAS
#include <QAndroidJniObject>
#include <QAndroidJniEnvironment>
#endif

namespace NfcTerminal {

class JniMethods
{
public:
    JniMethods() = delete;

    static QByteArray requestTransceive(const QByteArray &data, TagTechnologies tech);
    static bool isNfcSupported();
    static bool isNfcEnabled();
    static void requestEnableNfc();
    static QString getDeviceBrand();
    static QString getDeviceName();
    static QString getDeviceFingerprint();
    static QString getDeviceHardware();
    static QString getDeviceModel();
    static QString getDeviceProduct();
    static void logcat(const QString &msg);
    static QByteArray takeLogs();
    static bool isInternetAvailable();
    static QDateTime getAppInstallTime();
    static void requestTagsOnStartup();

#ifndef DO_NOT_USE_ANDROID_EXTRAS
    static void init(JNIEnv *env);
    static void deinit(JNIEnv *env);
#endif

private:
#ifndef DO_NOT_USE_ANDROID_EXTRAS
    struct ExceptionInfo
    {
        QString message;
        QString exceptionClass;
        QString stackTrace;
    };

    static void checkExceptions(QAndroidJniEnvironment &env);
    static void getExceptionInfo(const jthrowable &except, QAndroidJniEnvironment &env, ExceptionInfo &info);

    static jclass  m_activityClass;
    static jclass  m_throwableClass;
    static jclass  m_stackTraceElementClass;
    static jclass  m_javaClass;

    static jmethodID  m_requestNfcAMethod;
    static jmethodID  m_requestNfcBMethod;
    static jmethodID  m_requestIsoMethod;
    static jmethodID  m_isNfcSupportedMethod;
    static jmethodID  m_isNfcEnabledMethod;
    static jmethodID  m_requestEnableNfcMethod;
    static jmethodID  m_logcatMethod;
    static jmethodID  m_takeLogsMethod;
    static jmethodID  m_isInternetAvailableMethod;
    static jmethodID  m_getAppInstallMethod;
    static jmethodID  m_requestTagsOnStartup;

    static jmethodID  m_getClassMethod;
    static jmethodID  m_getNameMethod;
    static jmethodID  m_getMessageMethod;
    static jmethodID  m_getStackTraceMethod;
    static jmethodID  m_stackTraceToStringMethod;

    static QString  * m_deviceBrand;
    static QString  * m_deviceName;
    static QString  * m_deviceFingerprint;
    static QString  * m_deviceHardware;
    static QString  * m_deviceModel;
    static QString  * m_deviceProduct;

#endif


};

} // NfcTerminal namespace

#endif // JNIMETHODS_H
