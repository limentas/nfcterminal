import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

import "qrc:/qml/controls"

import NfcTerminal 1.0

ApplicationWindow {
    id: root
    visible: true
    width: 960
    height: 540
    title: qsTr("Nfc Terminal")

    AppTheme {id: theme}
    color: theme.backgroundColor

    property bool isPortraitOrientation: width <= height

    footer : Item {
        height: root.isPortraitOrientation ? 120 : 60
        width: parent.width
        InputCmdControl {
            id: inputCmd
            width: parent.width - sendButtonItem.width - 5
            model: ListModel {
                id: inputCmdHistory
            }
            favoritesCommandsModel: nfcTerminalApp.favoriteCommands
        }
        SendButton {
            id: sendButtonItem
            width: root.isPortraitOrientation ? parent.width : 200
            onNfcaClicked: sendCmd(TagTechnologies.ISO14443_3A)
            onNfcbClicked: sendCmd(TagTechnologies.ISO14443_3B)
            onIsoClicked: sendCmd(TagTechnologies.ISO14443_4)
            onFavoriteClicked: favoriteMenu.open()
            hasCard: nfcTerminalApp.cardInfo.hasCard
            nfcASupported: nfcTerminalApp.cardInfo.iso14443_3ASupported
            nfcBSupported: nfcTerminalApp.cardInfo.iso14443_3BSupported
            isoSupported: nfcTerminalApp.cardInfo.iso14443_4Supported
        }
        states: [
            State { //Portrait orientation
                when: root.isPortraitOrientation
                AnchorChanges {
                    target: inputCmd
                    anchors {
                        top: parent.top
                        right: parent.right
                        left: parent.left
                        bottom: parent.verticalCenter
                    }
                }
                AnchorChanges {
                    target: sendButtonItem
                    anchors {
                        top: parent.verticalCenter
                        left: parent.left
                        bottom: parent.bottom
                    }
                }
            },
            State { //Landscape orientation
                when: !root.isPortraitOrientation
                AnchorChanges {
                    target: inputCmd
                    anchors {
                        top: parent.top
                        right: sendButtonItem.left
                        left: parent.left
                        bottom: parent.bottom
                    }
                }
                AnchorChanges {
                    target: sendButtonItem
                    anchors {
                        top: parent.top
                        right: parent.right
                        bottom: parent.bottom
                    }
                }
            }
        ]
        transitions: Transition {
            AnchorAnimation {
                duration: 200
            }
        }
    }
    ColumnLayout {
        anchors.fill: parent
        spacing: 1
        TabBar {
            id: tabBarItem
            Layout.fillWidth: true
            background: Rectangle {
                color: theme.primaryColor
            }

            TabButton {
                text: qsTr("Exchange log")
            }
            TabButton {
                text: nfcTerminalApp.cardInfo.hasCard ? qsTr("Card info") : qsTr("No card detected")
            }
        }
        StackLayout {
            Layout.fillHeight: true
            Layout.fillWidth: true
            currentIndex: tabBarItem.currentIndex
            ExchangeLogPage {
                isPortraitOrientation: root.isPortraitOrientation
                onCmdCharChosen: appendCmdChar(ch)
                onLastCmdCharAboutToRemove: removeLast()
                onCmdCharsAboutToClear: clearCurrent()
            }
            CardInfoPage {
                isPortraitOrientation: root.isPortraitOrientation
                model: nfcTerminalApp.cardInfo
            }
        }
    }

    FavoriteMenu {
        id: favoriteMenu
        width: parent.width - 50
        height: parent.height - 50
        anchors.centerIn: parent
        model: nfcTerminalApp.favoriteCommands
        onFavoriteItemChosen: replaceCurrent(cmd)
    }

    function appendCmdChar(ch) {
        if (inputCmd.displayText.length % 3 == 2)
            inputCmd.displayText += " "
        inputCmd.displayText += ch
    }

    function removeLast() {
        if (inputCmd.displayText.length == 0)
            return;

        var charsToDelete = 1;
        if (inputCmd.displayText.length > 3 && inputCmd.displayText.length % 3 == 1)
            ++charsToDelete;

        inputCmd.displayText = inputCmd.displayText.substring(0, inputCmd.displayText.length-charsToDelete)
    }

    function clearCurrent() {
        inputCmd.displayText = ""
    }

    function replaceCurrent(cmd) {
        inputCmd.displayText = cmd
    }

    function sendCmd(tech) {
        if (inputCmd.displayText.length % 3 == 1)
            inputCmd.displayText += "0"
        var newCmd = inputCmd.displayText
        var newItem = {value: newCmd}
        var found = false;
        for(var idx = 0; idx < inputCmdHistory.count; ++idx)
            if (inputCmdHistory.get(idx).value === newCmd)
            {
                found = true
                break
            }
        if (!found)
        {
            inputCmdHistory.append(newItem)
            inputCmd.currentIndex = inputCmdHistory.count - 1
        }
        nfcTerminalApp.transceiveHex(inputCmd.displayText, tech)
    }

}
