import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

import "qrc:/qml/controls"

Dialog {
    id: editFavoriteDialogRoot
    title: "Edit favorite item"
    standardButtons: Dialog.Ok | Dialog.Cancel
    modal: true
    dim: true
    closePolicy: Popup.CloseOnEscape

    property int itemIndexToEdit : -1

    property string initNameValue : null
    property string initCommandValue : null

    property string resultNameValue : nameTextField.text

    onAboutToShow: {
        nameTextField.text = initNameValue
        nameCommandField.text = initCommandValue
    }

    GridLayout {
        anchors.fill: parent
        columns: 2
        columnSpacing: 15
        Text {
            text: "Name:"
            font.pointSize: 13
            font.family: theme.fontFamily
            Layout.alignment: Qt.AlignVCenter | Qt.AlignLeft
            AppTheme {
                id: theme
            }
        }
        TextField {
            id: nameTextField
            Layout.fillWidth: true
            Layout.preferredWidth: implicitWidth + 10
            Layout.alignment: Qt.AlignVCenter | Qt.AlignLeft
            font.family: theme.fontFamily
        }

        Text {
            text: "Command:"
            font.pointSize: 13
            font.family: theme.fontFamily
            Layout.alignment: Qt.AlignVCenter | Qt.AlignLeft
        }
        Text {
            id: nameCommandField
            Layout.fillWidth: true
            Layout.maximumWidth: 1000
            Layout.preferredWidth: implicitWidth + 10
            font.family: "Droid Sans Mono"
            font.pointSize: 13
            Layout.alignment: Qt.AlignVCenter | Qt.AlignLeft
            fontSizeMode: Text.HorizontalFit
            minimumPointSize: 11
            clip: true
        }
    }
}
