import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.12

import "qrc:/qml/controls"

import NfcTerminal 1.0

Item {
    id: exchangeLogRoot
    property string model: null
    //property var model: null
    AppTheme { id: theme }

    ScrollView {
        anchors.fill: parent
        anchors.margins: 0
        ScrollBar.vertical.policy: ScrollBar.AlwaysOn
        ScrollBar.horizontal.policy: ScrollBar.AsNeeded
        TextArea {
            text: exchangeLogRoot.model
            selectByMouse : false
            readOnly: true
            font.family: "Droid Sans Mono"
            font.pointSize: 12
            font.bold: false
            color: theme.textColor
        }

/*        ListView {
            id: listViewItem
            anchors.fill: parent
            flickableDirection: Flickable.HorizontalAndVerticalFlick
            boundsBehavior: Flickable.StopAtBounds
            model: exchangeLogRoot.model
            snapMode: ListView.SnapToItem

            delegate:
                RowLayout {
                onWidthChanged: if (width > listViewItem.contentWidth)
                                    listViewItem.contentWidth = width
                height: 20
                SelectableText {
                    width: 100
                    Layout.fillHeight: true
                    text: Qt.formatDateTime(model.time, "hh:mm:ss.zzz")
                }
                SelectableText {
                    Layout.fillHeight: true
                    text: model.direction === ExchangeDirection.ToCard
                          ? techToString(model.technology)
                          : "  "
                }
                SelectableText {
                    width: 20
                    Layout.fillHeight: true
                    text: model.direction === ExchangeDirection.ToCard ? ">>" : "<<"
                }
                SelectableText {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    text: model.display
                }

                function techToString(tech) {
                    switch(tech) {
                    case TagTechnologies.ISO14443_3A:
                        return "3A";
                    case TagTechnologies.ISO14443_3B:
                        return "3B";
                    case TagTechnologies.ISO14443_4:
                        return "4 ";
                    }
                }
            }

            onCountChanged: {
                currentIndex = count - 1
            }
        }*/
    }
}
