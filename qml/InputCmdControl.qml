import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtGraphicalEffects 1.12

import "qrc:/qml/controls"

ComboBox {
    id: inputCmdControlRoot
    AppTheme { id: theme }
    textRole: "value"
    Material.background: theme.backgroundColor
    onCurrentTextChanged: {
        editText = displayText = currentText
    }
    onDisplayTextChanged: {
        if (editText !== displayText) editText = displayText
    }
    onEditTextChanged: {
        if (editText !== displayText) displayText = editText
    }
    editable: false

    property var favoritesCommandsModel: null

    delegate: ItemDelegate {
        height: inputCmdControlRoot.height
        width: parent.width
        highlighted: inputCmdControlRoot.highlightedIndex === index

        Text {
            height: parent.height
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.top: parent.top
            anchors.right: favButtonRec.left
            verticalAlignment: Text.AlignVCenter
            text: modelData
            font.pointSize: 12
            font.family: theme.fontFamily
            fontSizeMode: Text.HorizontalFit
            minimumPointSize: 11
            clip: true
        }

        Image {
            id: favButtonRec
            height: parent.height / 2
            width: height
            anchors.right: parent.right
            anchors.rightMargin: 5
            anchors.verticalCenter: parent.verticalCenter
            source: checked ? "qrc:/icons/favorite.svg" : "qrc:/icons/unfavorite.svg"
            property bool checked: inputCmdControlRoot.favoritesCommandsModel.isInFavorites(modelData)
            smooth: true
            mipmap: true

            Connections {
                id: checkedUpdateConnection
                target: inputCmdControlRoot.favoritesCommandsModel
                onRowsRemoved: favButtonRec.checked
                               = inputCmdControlRoot.favoritesCommandsModel.isInFavorites(modelData)
            }
        }

        MouseArea {
            anchors.centerIn: favButtonRec
            height: parent.height
            width: height
            onClicked: {
                if (favButtonRec.checked)
                {
                    checkedUpdateConnection.enabled = false
                    inputCmdControlRoot.favoritesCommandsModel.removeItem(modelData)
                    checkedUpdateConnection.enabled = true
                    favButtonRec.checked = false
                }
                else
                {
                    inputCmdControlRoot.favoritesCommandsModel.addItem(modelData, "");
                    favButtonRec.checked = true
                }
            }
        }
    }
}
