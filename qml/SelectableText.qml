import QtQuick 2.0

import "qrc:/qml/controls"

TextInput {
    readOnly: true
    selectByMouse: true
    font.family: "Droid Sans Mono"
    font.pointSize: 13
    font.bold: false
    AppTheme {id: theme}
    color: theme.textColor
    clip: true
    property bool completed: false
    Component.onCompleted: completed = true
    /*onWidthChanged: {
        if (implicitWidth > 0.92 * width && completed)
        {
            console.log("------------------------------------------------------" + font.pointSize);
            font.pointSize = 10//Math.max(font.pointSize - 1, 8)
        }
        else if (implicitWidth < 0.5 * width && completed)
        {
            console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++" + font.pointSize);
            font.pointSize = 15//Math.min(font.pointSize + 1, 16)
        }
    }*/
}
