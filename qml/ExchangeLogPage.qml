import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

import "qrc:/qml/controls"

Item {
    id: exchangeLogPageRoot

    property bool isPortraitOrientation
    signal cmdCharChosen(var ch)
    signal lastCmdCharAboutToRemove()
    signal cmdCharsAboutToClear()

    ExchangeLog {
        id: logItem
        anchors.margins: 0
        anchors.leftMargin: 5
        model: nfcTerminalApp.exchangeLogString
    }

    HexKeyboard {
        id: keyboardItem
        width: isHidden ? 0 : exchangeLogPageRoot.isPortraitOrientation ? parent.width : 0.38 * parent.width
        height: isHidden ? 0 : exchangeLogPageRoot.isPortraitOrientation ? 0.38 * parent.height : parent.height - 10
        property bool isHidden: false
        onButton0Clicked: exchangeLogPageRoot.cmdCharChosen("0")
        onButton1Clicked: exchangeLogPageRoot.cmdCharChosen("1")
        onButton2Clicked: exchangeLogPageRoot.cmdCharChosen("2")
        onButton3Clicked: exchangeLogPageRoot.cmdCharChosen("3")
        onButton4Clicked: exchangeLogPageRoot.cmdCharChosen("4")
        onButton5Clicked: exchangeLogPageRoot.cmdCharChosen("5")
        onButton6Clicked: exchangeLogPageRoot.cmdCharChosen("6")
        onButton7Clicked: exchangeLogPageRoot.cmdCharChosen("7")
        onButton8Clicked: exchangeLogPageRoot.cmdCharChosen("8")
        onButton9Clicked: exchangeLogPageRoot.cmdCharChosen("9")
        onButtonAClicked: exchangeLogPageRoot.cmdCharChosen("A")
        onButtonBClicked: exchangeLogPageRoot.cmdCharChosen("B")
        onButtonCClicked: exchangeLogPageRoot.cmdCharChosen("C")
        onButtonDClicked: exchangeLogPageRoot.cmdCharChosen("D")
        onButtonEClicked: exchangeLogPageRoot.cmdCharChosen("E")
        onButtonFClicked: exchangeLogPageRoot.cmdCharChosen("F")
        onButtonBackspaceClicked: removeLast()
        onButtonClearClicked: clearCurrent()

        focus: true
        //onFocusChanged: focus = true
        Keys.onDigit0Pressed: exchangeLogPageRoot.cmdCharChosen("0")
        Keys.onDigit1Pressed: exchangeLogPageRoot.cmdCharChosen("1")
        Keys.onDigit2Pressed: exchangeLogPageRoot.cmdCharChosen("2")
        Keys.onDigit3Pressed: exchangeLogPageRoot.cmdCharChosen("3")
        Keys.onDigit4Pressed: exchangeLogPageRoot.cmdCharChosen("4")
        Keys.onDigit5Pressed: exchangeLogPageRoot.cmdCharChosen("5")
        Keys.onDigit6Pressed: exchangeLogPageRoot.cmdCharChosen("6")
        Keys.onDigit7Pressed: exchangeLogPageRoot.cmdCharChosen("7")
        Keys.onDigit8Pressed: exchangeLogPageRoot.cmdCharChosen("8")
        Keys.onDigit9Pressed: exchangeLogPageRoot.cmdCharChosen("9")
        Keys.onReturnPressed: sendButtonItem.send()
        Keys.onPressed: {
            switch(event.key) {
            case Qt.Key_A:
                exchangeLogPageRoot.cmdCharChosen("A");
                break;
            case Qt.Key_B:
                exchangeLogPageRoot.cmdCharChosen("B");
                break;
            case Qt.Key_C:
                exchangeLogPageRoot.cmdCharChosen("C");
                break;
            case Qt.Key_D:
                exchangeLogPageRoot.cmdCharChosen("D");
                break;
            case Qt.Key_E:
                exchangeLogPageRoot.cmdCharChosen("E");
                break;
            case Qt.Key_F:
                exchangeLogPageRoot.cmdCharChosen("F");
                break;
            case Qt.Key_Backspace:
                removeLast();
                break;
            }
        }
    }

    RoundButton {
        z: 1
        height: 44
        width: height
        radius: height
        icon.source: keyboardItem.isHidden ? "qrc:/icons/maximize.svg" : "qrc:/icons/minimize.svg"
        anchors.right: parent.right
        anchors.rightMargin: -2
        anchors.top: keyboardItem.top
        anchors.topMargin: keyboardItem.isHidden ? - height : -10
        onClicked: keyboardItem.isHidden = !keyboardItem.isHidden
    }

    states: [
        State { //Portrait orientation
            when: exchangeLogPageRoot.isPortraitOrientation
            AnchorChanges {
                target: logItem
                anchors {
                    top: parent.top
                    right: parent.right
                    left: parent.left
                    bottom: keyboardItem.top
                }
            }
            AnchorChanges {
                target: keyboardItem
                anchors {
                    left: parent.left
                    bottom: parent.bottom
                }
            }
        },
        State { //Landscape orientation
            when: !exchangeLogPageRoot.isPortraitOrientation
            AnchorChanges {
                target: logItem
                anchors {
                    top: parent.top
                    right: keyboardItem.left
                    left: parent.left
                    bottom: parent.bottom
                }
            }
            AnchorChanges {
                target: keyboardItem
                anchors {
                    right: parent.right
                    bottom: parent.bottom
                }
            }
        }
    ]
    transitions: Transition {
        AnchorAnimation {
            duration: 200
        }
    }
}
