import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.12

import "qrc:/qml/controls"

import NfcTerminal 1.0

Popup {
    id: favoriteMenuItemRoot
    property var model : null

    signal favoriteItemChosen(var cmd)

    AppTheme {id: theme}
    ScrollView {
        anchors.fill: parent
        ScrollBar.vertical.policy: ScrollBar.AsNeeded
        ScrollBar.horizontal.policy: ScrollBar.AsNeeded
        ListView {
            anchors.fill: parent
            model: favoriteMenuItemRoot.model
            clip: true
            delegate: ItemDelegate {
                height: 60
                width: parent.width
                onClicked: {
                    favoriteMenuItemRoot.favoriteItemChosen(cmd)
                    favoriteMenuItemRoot.close()
                }
                RowLayout {
                    anchors.fill: parent
                    anchors.leftMargin: 10
                    anchors.rightMargin: 10
                    ColumnLayout {
                        height: implicitHeight
                        Layout.alignment: Qt.AlignVCenter
                        Layout.fillWidth: true
                        Layout.maximumWidth: Number.POSITIVE_INFINITY
                        Layout.minimumWidth: 50
                        clip: true

                        AppText {
                            text: name
                            font.pointSize: 13
                        }
                        AppText {
                            text: cmd
                            Layout.fillWidth: true
                            color: theme.altTextColor
                            font.bold: false
                            Layout.leftMargin: 10
                            Layout.rightMargin: 10
                            fontSizeMode: Text.HorizontalFit
                            minimumPointSize: 11
                            font.pointSize: 12
                        }
                    }

                    Button {
                        width: 50
                        height: parent.height
                        icon.source: "qrc:/icons/edit.svg"
                        onClicked:
                        {
                            editDialog.initNameValue = name
                            editDialog.initCommandValue = cmd
                            editDialog.itemIndexToEdit = index
                            editDialog.open()
                        }
                    }

                    Button {
                        width: 50
                        height: parent.height
                        icon.source: "qrc:/icons/delete.svg"
                        onClicked: favoriteMenuItemRoot.model.removeItem(index)
                    }
                }
            }
        }
    }

    EditFavoriteDialog {
        id: editDialog
        anchors.centerIn: parent
        width: Math.min(implicitWidth, parent.width - 10)
        onAccepted: {
            favoriteMenuItemRoot.model.editItem(itemIndexToEdit, resultNameValue)
        }
    }
}
