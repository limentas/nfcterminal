import QtQuick 2.0

import "qrc:/qml/controls"

Text {
    font.family: "Roboto"
    font.bold: true
    AppTheme {id: theme}
    color: theme.textColor

    property bool fontSizeDecreased: false
    onLineCountChanged: {
        if (!fontSizeDecreased) {
            fontSizeDecreased = true
            font.pointSize = Math.ceil(0.85 * font.pointSize)
        }
    }
}
