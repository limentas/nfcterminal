import QtQuick 2.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.12

import "qrc:/qml/controls"

Pane {
    anchors.margins: 5
    anchors.topMargin: 10
    id: hexKeyboardRoot
    signal button0Clicked()
    signal button1Clicked()
    signal button2Clicked()
    signal button3Clicked()
    signal button4Clicked()
    signal button5Clicked()
    signal button6Clicked()
    signal button7Clicked()
    signal button8Clicked()
    signal button9Clicked()
    signal buttonAClicked()
    signal buttonBClicked()
    signal buttonCClicked()
    signal buttonDClicked()
    signal buttonEClicked()
    signal buttonFClicked()
    signal buttonSpaceClicked()
    signal buttonBackspaceClicked()
    signal buttonClearClicked()

    AppTheme {id: theme}

    Material.elevation: 2

    GridLayout {
        anchors.fill: parent
        anchors.margins: -5
        columns: 5
        rows: 4
        rowSpacing: 0
        columnSpacing: 0

        //1st row
        HexKeyboardButton {
            id: firstButton
            text: "1"
            Layout.fillHeight: true
            Layout.fillWidth: true
            onClicked: hexKeyboardRoot.button1Clicked()
        }

        HexKeyboardButton {
            text: "2"
            Layout.fillHeight: true
            Layout.fillWidth: true
            onClicked: hexKeyboardRoot.button2Clicked()
        }

        HexKeyboardButton {
            text: "3"
            Layout.fillHeight: true
            Layout.fillWidth: true
            onClicked: hexKeyboardRoot.button3Clicked()
        }

        HexKeyboardButton {
            icon.source: "qrc:/icons/backspace.svg"
            icon.width: width
            icon.height: height
            Layout.fillHeight: true
            Layout.fillWidth: true
            onClicked: hexKeyboardRoot.buttonBackspaceClicked()
            Material.background: "#e6cfcf"
        }

        HexKeyboardButton {
            icon.source: "qrc:/icons/clear.svg"
            icon.width: width
            icon.height: height
            Layout.fillHeight: true
            Layout.fillWidth: true
            onClicked: hexKeyboardRoot.buttonClearClicked()
            Material.background: "#e6cfcf"
        }

        //2nd row
        HexKeyboardButton {
            text: "4"
            Layout.fillHeight: true
            Layout.fillWidth: true
            onClicked: hexKeyboardRoot.button4Clicked()
        }

        HexKeyboardButton {
            text: "5"
            Layout.fillHeight: true
            Layout.fillWidth: true
            onClicked: hexKeyboardRoot.button5Clicked()
        }

        HexKeyboardButton {
            text: "6"
            Layout.fillHeight: true
            Layout.fillWidth: true
            onClicked: hexKeyboardRoot.button6Clicked()
        }

        HexKeyboardButton {
            text: "A"
            Layout.fillHeight: true
            Layout.fillWidth: true
            onClicked: hexKeyboardRoot.buttonAClicked()
        }

        HexKeyboardButton {
            text: "B"
            Layout.fillHeight: true
            Layout.fillWidth: true
            onClicked: hexKeyboardRoot.buttonBClicked()
        }

        //3rd row
        HexKeyboardButton {
            text: "7"
            Layout.fillHeight: true
            Layout.fillWidth: true
            onClicked: hexKeyboardRoot.button7Clicked()
        }

        HexKeyboardButton {
            text: "8"
            Layout.fillHeight: true
            Layout.fillWidth: true
            onClicked: hexKeyboardRoot.button8Clicked()
        }

        HexKeyboardButton {
            text: "9"
            Layout.fillHeight: true
            Layout.fillWidth: true
            onClicked: hexKeyboardRoot.button9Clicked()
        }

        HexKeyboardButton {
            text: "C"
            Layout.fillHeight: true
            Layout.fillWidth: true
            onClicked: hexKeyboardRoot.buttonCClicked()
        }

        HexKeyboardButton {
            text: "D"
            Layout.fillHeight: true
            Layout.fillWidth: true
            onClicked: hexKeyboardRoot.buttonDClicked()
        }

        //4th row
        HexKeyboardButton {
            text: "0"
            Layout.columnSpan: 3
            Layout.fillHeight: true
            Layout.fillWidth: true
            onClicked: hexKeyboardRoot.button0Clicked()
        }

        HexKeyboardButton {
            text: "E"
            Layout.fillHeight: true
            Layout.fillWidth: true
            onClicked: hexKeyboardRoot.buttonEClicked()
        }

        HexKeyboardButton {
            text: "F"
            Layout.fillHeight: true
            Layout.fillWidth: true
            onClicked: hexKeyboardRoot.buttonFClicked()
        }
    }
}
