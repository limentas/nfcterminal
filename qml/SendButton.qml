import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

import "qrc:/qml/controls"

RowLayout {
    id: sendButtonRoot
    spacing: 0
    signal nfcaClicked()
    signal nfcbClicked()
    signal isoClicked()
    signal favoriteClicked()
    property bool hasCard: false
    property bool isNfcA: false
    property bool isNfcB: false
    property bool isIso: true
    property bool nfcASupported: false
    property bool nfcBSupported: false
    property bool isoSupported: false

    onHasCardChanged: {
        if (hasCard) {
            if (isIso && isoSupported || isNfcA && nfcASupported || isNfcB && nfcBSupported)
                return //карта поддерживает прошлый выбор - оставляем как было
            if (isoSupported)
                isIso = true
            else if (nfcASupported)
                isNfcA = true
            else if (nfcBSupported)
                isNfcB = true
        }
    }

    function send() {
        if (sendButtonRoot.isNfcA)
            nfcaClicked();
        else if (sendButtonRoot.isNfcB)
            nfcbClicked();
        else if (sendButtonRoot.isIso)
            isoClicked();
    }

    AppTheme {
        id: theme
    }

    Button {
        Layout.preferredWidth: parent.width / 4
        Layout.fillHeight: true
        Layout.rightMargin: 5
        onClicked: sendButtonRoot.favoriteClicked()
        icon.source: "qrc:/icons/unfavorite.svg"
    }

    Button {
        text: "Send"
        Layout.preferredWidth: 2 * parent.width / 4
        Layout.fillHeight: true
        onClicked: sendButtonRoot.send()
        enabled: sendButtonRoot.hasCard
        font.bold: true
    }
    Button {
        text: "V"
        Layout.preferredWidth: parent.width / 4
        Layout.fillHeight: true
        onClicked: contextMenu.popup()
        enabled: sendButtonRoot.hasCard
        font.bold: true
    }

    Menu {
        id: contextMenu
        MenuItem {
            text: "ISO14443-3A (NFC-A)"
            checkable : true
            checked : sendButtonRoot.isNfcA
            ButtonGroup.group: contextMenuButtonGroup
            onCheckedChanged: sendButtonRoot.isNfcA = checked
            enabled: sendButtonRoot.nfcASupported
        }
        MenuItem {
            text: "ISO14443-3B (NFC-B)"
            checkable : true
            checked : sendButtonRoot.isNfcB
            ButtonGroup.group: contextMenuButtonGroup
            onCheckedChanged: sendButtonRoot.isNfcB = checked
            enabled: sendButtonRoot.nfcBSupported
        }
        MenuItem {
            text: "ISO14443-4 (ISO-DEP)"
            checkable : true
            checked : sendButtonRoot.isIso
            ButtonGroup.group: contextMenuButtonGroup
            onCheckedChanged: sendButtonRoot.isIso = checked
            enabled: sendButtonRoot.isoSupported
        }

        ButtonGroup {
            id: contextMenuButtonGroup
        }
    }
}
