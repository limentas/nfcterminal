import QtQuick 2.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQuick.Controls 1.4

import "qrc:/qml/controls"

Item {
    id: cardInfoPageRoot
    property var model : null
    property bool isPortraitOrientation: false

    property bool isNfcASupported: model.iso14443_3ASupported
    property bool isNfcBSupported: model.iso14443_3BSupported
    property bool isNfc4Supported: model.iso14443_4Supported

    GridLayout {
        anchors.fill: parent
        anchors.margins: 3
        anchors.leftMargin: 7
        columns: 2
        SelectableText {
            Layout.fillWidth: true
            visible: cardInfoPageRoot.isNfcASupported | cardInfoPageRoot.isNfcBSupported
                     | cardInfoPageRoot.isNfc4Supported
            text: qsTr("UID:")
        }
        SelectableText {
            Layout.fillWidth: true
            visible: cardInfoPageRoot.isNfcASupported | cardInfoPageRoot.isNfcBSupported
                     | cardInfoPageRoot.isNfc4Supported
            text: qsTr("%1 (hex)").arg(cardInfoPageRoot.model.uidHex)

        }

        SelectableText {
            Layout.fillWidth: true
            visible: cardInfoPageRoot.isNfcASupported
            text: qsTr("ATQA:")
        }
        SelectableText {
            Layout.fillWidth: true
            visible: cardInfoPageRoot.isNfcASupported
            text: qsTr("%1 (0x%2)").arg(cardInfoPageRoot.model.atqa).arg(cardInfoPageRoot.model.atqa.toString(16))
        }

        SelectableText {
            Layout.fillWidth: true
            visible: cardInfoPageRoot.isNfcASupported
            text: qsTr("SAK:")
        }
        SelectableText {
            Layout.fillWidth: true
            visible: cardInfoPageRoot.isNfcASupported
            text: qsTr("%1 (0x%2)").arg(cardInfoPageRoot.model.sak).arg(cardInfoPageRoot.model.sak.toString(16))
        }

        SelectableText {
            Layout.fillWidth: true
            visible: cardInfoPageRoot.isNfcASupported && cardInfoPageRoot.isNfc4Supported
            text: qsTr("Historical bytes:")
        }
        SelectableText {
            Layout.fillWidth: true
            visible: cardInfoPageRoot.isNfcASupported && cardInfoPageRoot.isNfc4Supported
            text: qsTr("%1 (hex)").arg(cardInfoPageRoot.model.historicalBytesHex)
        }

        SelectableText {
            Layout.fillWidth: true
            visible: cardInfoPageRoot.isNfcBSupported
            text: qsTr("Application Data:")
        }
        SelectableText {
            Layout.fillWidth: true
            visible: cardInfoPageRoot.isNfcBSupported
            text: qsTr("%1 (hex)").arg(cardInfoPageRoot.model.applicationDataHex)
        }

        SelectableText {
            Layout.fillWidth: true
            visible: cardInfoPageRoot.isNfcBSupported
            text: qsTr("Protocol info:")
        }
        SelectableText {
            Layout.fillWidth: true
            visible: cardInfoPageRoot.isNfcBSupported
            text: qsTr("%1 (hex)").arg(cardInfoPageRoot.model.protocolInfoHex)
        }

        SelectableText {
            Layout.fillWidth: true
            visible: cardInfoPageRoot.isNfcBSupported
            text: qsTr("Hi layer resp.:")
        }
        SelectableText {
            Layout.fillWidth: true
            visible: cardInfoPageRoot.isNfcBSupported
            text: qsTr("%1 (hex)").arg(cardInfoPageRoot.model.highLayerResponseHex)
        }
    }
}
