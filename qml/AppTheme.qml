import QtQuick 2.0

Item {
    readonly property color backgroundColor: "#fffde7"
    readonly property color titlePanelColor: "#91C342"
    readonly property color primaryColor: "#cfd8dc"
    readonly property color textColor: "#F0000000"
    readonly property color altTextColor: "#807D75"
    readonly property color borderColor: "#BFBCB0"
    readonly property color accentColor: "#ff8f00"
    readonly property string fontFamily: "Roboto"
}
