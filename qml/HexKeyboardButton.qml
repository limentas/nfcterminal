import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

import "qrc:/qml/controls"

RoundButton {
    AppTheme {id: theme}

    implicitHeight: 10
    implicitWidth: 10
    radius: 6
    font.bold: false
    font.pixelSize: height * 0.5
    font.family: theme.fontFamily
    Material.elevation: 2
    Material.background: theme.primaryColor
}
